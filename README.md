# DrivenData: DengAI Predicting Disease Spread

Códigos associados com a competição da DrivenData "DengAI Competition", cujo objetivo é mapear a evolução de casos de dengue em duas cidades tropicais.
URL da competição: https://www.drivendata.org/competitions/44/dengai-predicting-disease-spread/ 
Melhor posição no ranking foi Top 5%, em 02/06/2017.

# Tecnologias utilizadas
- Pandas e Numpy (Manipulação de dados)
- Matplotlib e Seaborn (Visualização de dados)
- Sklearn (Machine Learning)