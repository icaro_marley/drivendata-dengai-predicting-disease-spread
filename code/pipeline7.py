# -*- coding: utf-8 -*-

# -*- coding: utf-8 -*-
"""
Created on Sun May 14 00:49:55 2017

@author: icaromarley5
"""


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats
import seaborn as sns
import winsound
from sklearn.externals import joblib
from sklearn.ensemble import RandomForestRegressor, ExtraTreesRegressor, GradientBoostingRegressor
from sklearn import preprocessing
from sklearn.preprocessing import PolynomialFeatures
from sklearn import linear_model
from sklearn.preprocessing import PolynomialFeatures,StandardScaler
from sklearn.model_selection import GridSearchCV, cross_val_score, KFold
from sklearn.metrics import mean_absolute_error
from sklearn.svm import SVR
from sklearn.svm import NuSVR,LinearSVR
from sklearn.ensemble import AdaBoostRegressor,BaggingRegressor
import statsmodels.api as sm
from statsmodels.tsa.arima_model import ARIMA

#np.set_printoptions(threshold='nan')
#pd.set_option('display.max_rows', None)
#preprocess data and returns 1 df
np.set_printoptions(threshold=np.nan)
def pre_process_data(df,response=None,weeks_before=12): 
    #splitting to model the 2 cities
    df = df.copy()

    columns = ['week_start_date']
    df['week_start_date'] = pd.to_datetime(df['week_start_date'])
    df.set_index('week_start_date',inplace=True)
    #df.drop(columns,axis=1,inplace=True)
    

    
    columns = df.columns.values
    if response != None:
        columns = np.delete(columns,df.columns.get_loc(response))
    columns = np.delete(columns,df.columns.get_loc('city'))
        
    #print(columns)
    old_len_columns = len(columns) 

    #split the dfs
    df_train_iq = df[df['city'] == 'iq'].copy()
    df_train_sj = df[df['city'] == 'sj'].copy()    
    df_train_iq.drop('city',axis=1,inplace=True)
    df_train_sj.drop('city',axis=1,inplace=True)    
    
    dfs_train = [df_train_iq,df_train_sj]
    
    for df in dfs_train:
        #dealing with na values 
        df.fillna(method='ffill',inplace=True) 
        
        for n in range(1,weeks_before+1):
            for column in columns:
                df[column+"_"+ str(n)] = df[column].shift(n)
    
        #dealing with na values for the shift operation
        df.fillna(method='bfill',inplace=True)#columns shifted 
    
        

        
    return dfs_train,old_len_columns


#reading training files
path = "../modified data/"
features_path = path + "dengue_features_train.csv"
labels_path = path + "dengue_labels_train.csv"

df_feat = pd.read_csv(features_path)
df_lab = pd.read_csv(labels_path)

#merge
df_merge = pd.merge(df_feat,df_lab,on=["city","year","weekofyear"])


#selecting response
response = "total_cases"

dfs_train,old_len_columns = pre_process_data(df_merge,response)


'''
#hist plot
f, axarr = plt.subplots(1,2,sharex=True,sharey=True,figsize=(7,4))
index = 0 
for city,df_train in zip(["Iquitos","San Juan"],dfs_train):
    f.subplots_adjust(hspace=.3)
    axarr[index].hist(df_train[response], 50)
    axarr[index].set_title(city)
    index+=1
plt.suptitle("Histogram of total dengue fever cases")
plt.show()


#line plot
f, axarr = plt.subplots(1,2,sharey=True,sharex=True,figsize=(7,4))
index = 0 
for city,df_train in zip(["Iquitos","San Juan"],dfs_train):
    #df_train.set_index(['weekofyear','year'],inplace=True)
    f.subplots_adjust(hspace=.3)
    axarr[index].plot(df_train[response])
    axarr[index].set_title(city)
    index+=1
    #df_train.reset_index()
plt.suptitle("Line Plot of total dengue fever cases")
plt.show()

#correlations
for city,df_train in zip(["Iquitos","San Juan"],dfs_train):
    corrmat = df_train.corr()
    fig = plt.figure(figsize=(50, 10), dpi=100)
    ticks = np.arange(len(corrmat['total_cases']))
    plt.plot(ticks,corrmat['total_cases'])
    labels = corrmat['total_cases'].index.values
    plt.xticks(ticks, labels, rotation='vertical')
    plt.title(city)
    plt.show()

for method in ['pearson','spearman','kendall']:
    print(method.upper())
    for df_train,name in zip(dfs_train,['IQUITOS','SAN JUAN']):
        print(name)
        corr = df_train.corr(method=method)['total_cases']
        corr.drop('total_cases',inplace=True)
        print(corr.sort_values(ascending = False)[:50])
    print()

     


#usar para hexbin tbm
#scatter plot predictor x reponse by medition week 
len_side = 6
predictors = np.delete(df_train.columns,df_train.columns.get_loc(response))
predictors_len = len(predictors)

for city,df_train in zip([["Iquitos","San Juan"][1]],[dfs_train[1]]):
    df_train = df_train.copy()
    step = old_len_columns
    limit_inf = 0
    limit_max = step
    week = 0
    while (limit_max<=predictors_len):
        index = 0
        side = 0
        selected = predictors[limit_inf:limit_max]
        
        f, axarr = plt.subplots(int(len(selected)/len_side) + 1,len_side,
                                figsize=(19,14),sharey=True)
        f.subplots_adjust(hspace=.5)
        for predictor in selected:
            x = df_train[predictor]#.apply(lambda x:np.log(x))
            y = df_train[response]#.apply(lambda x: np.power(x,1/4))
            df_train[predictor] = x
            df_train[response] = y
            #print(predictor,index,side)
            axarr[index,side].scatter(x,y)#,cmap='inferno')
            axarr[index,side].set_xlabel(predictor)
            corr = scipy.stats.pearsonr(x,y)
            axarr[index,side].set_title("Pearson R {:.2f} P {:.2f}".format(corr[0],corr[1]))
            side+=1
            if side > len_side - 1:
                side = 0
                index+=1   
        plt.suptitle(city + ": total dengue fever cases by predictors week " + str(week))
        plt.show()  
        
        #print(limit_inf,limit_max,selected,step)
        limit_inf = limit_max
        limit_max += step
        week+= -1
        
    for method in ['pearson','spearman','kendall']:
        print(method.upper())
        print(city)
        corr = df_train.corr(method=method)['total_cases']
        corr.drop('total_cases',inplace=True)
        print(corr.sort_values(ascending = False)[:20])
        print()
        
'''

#TRAINING
def train(x,y,estimator,params):
    outer_cv = KFold(n_splits=10, shuffle=False)
    inner_cv = KFold(n_splits=5, shuffle=False)
    
    #non nested
    clf = GridSearchCV(estimator, params,scoring="neg_mean_absolute_error",cv=inner_cv)#,verbose=10000000000)
    clf.fit(x,y)
    tab = "   "
    print('non nested CV')
    print(tab,'best params',clf.best_params_)
    print(tab,"score",clf.best_score_)
    print(tab,"std",clf.cv_results_['std_test_score'])
    model = clf.best_estimator_
    pred = model.predict(x)
    true = y
    print(tab,"full dataset score", - mean_absolute_error(true,pred))


    #nested
    clf = GridSearchCV(estimator, params,scoring="neg_mean_absolute_error",cv=inner_cv)
    nested_cv = cross_val_score(clf, X=x, y=y, cv=outer_cv)
    print('nested CV')
    print(tab,"score",nested_cv.mean())
    print(tab,"std",nested_cv.std())

    return model


'''
IQUITOS 

'''


print('\nIQUITOS MODEL TRAINING\n')
df_train = dfs_train[0]
def back_transform_iq_y(values):
    return values.round(0).astype(int)


#return x
def pre_process_iq(df,get_response=True):
    explanatory = [
    'reanalysis_specific_humidity_g_per_kg_3',
    'weekofyear_10',
    ]
    
    x = df[explanatory].copy()
    #scaler = preprocessing.StandardScaler().fit(x)
    #x = scaler.transform(x) 
    #poly = PolynomialFeatures(degree=3)
    #x = poly.fit_transform(x)
    
    if get_response:
        y = df[response].copy()
        return x,y

    return x



x_iq,y_iq = pre_process_iq(df_train)
x = x_iq
y = y_iq


#estimator = RandomForestRegressor(random_state=100,n_jobs = -1)
#-6.75011223048 on reanalysis_min_air_temp_k well fitted 



#estimator = linear_model.LinearRegression()
#-6.09716923523     'weekofyear_11','weekofyear_12','weekofyear_8','reanalysis_specific_humidity_g_per_kg_3','reanalysis_dew_point_temp_k',

#estimator = GradientBoostingRegressor(random_state=100, n_estimators=12)
#-6.18784649128 reanalysis_min_air_temp_k weekofyear_10 (lower overfitting)



estimator = SVR()
#-5.60065916859  on weekofyear_10 reanalysis_specific_humidity_g_per_kg_3 kernel rbf


params = {

}

# Non_nested parameter search and scoring
best_iq_model = train(x,y,estimator,params)






'''
SAN JUAN 
'''

print('\nSAN JUAN MODEL TRAINING\n')
df_train = dfs_train[1]
def back_transform_sj_y(values):
    return values.round(0).astype(int)

#return x
def pre_process_sj(df,get_response=True):
    explanatory = [
 'station_min_temp_c_10',
 'station_avg_temp_c_12',
 'station_min_temp_c_8',
 'station_min_temp_c_7',
 'station_avg_temp_c_5',
     ]
    
    
    explanatory = [
     'station_avg_temp_c_12',
     'station_min_temp_c_8',
     'station_min_temp_c_7',
     'station_avg_temp_c_5',        
    
    'station_min_temp_c_10',
    #'station_max_temp_c_11',
    'reanalysis_specific_humidity_g_per_kg',
    'reanalysis_dew_point_temp_k',
    ]
    
    
    x = df[explanatory].copy()
    '''
    scaler = preprocessing.StandardScaler().fit(x)
    x = scaler.transform(x) 
    
    poly = PolynomialFeatures(degree=3)
    x = poly.fit_transform(x)
    '''
    if get_response:
        y = df[response].copy()
        return x,y
    return x

x_sj,y_sj = pre_process_sj(df_train)
x = x_sj
y = y_sj

    


estimator = GradientBoostingRegressor(random_state=100,n_estimators=21,max_depth=2,presort=False,criterion='mae')



#estimator = SVR(kernel='rbf')#nice   
#estimator = SVR(kernel='linear')#nice     

#estimator = NuSVR() 
               
#estimator = LinearSVR(random_state=100,max_iter=4000)

#estimator = AdaBoostRegressor(estimator,random_state=100,n_estimators=500)

#estimator = RandomForestRegressor(random_state=100,n_jobs = -1,n_estimators=50) #ovefitting or underfitting

                                  
#estimator = RandomForestRegressor(random_state=100,n_jobs = -1,n_estimators=i) #ovefitting or underfitting
#estimator = GradientBoostingRegressor(random_state=100) #overfitting

#estimator = linear_model.LinearRegression()  #high bias
#estimator = GradientBoostingRegressor(random_state=100) #overfitting



params = {
#'n_estimators':np.append(np.arange(50,100),25)
#'max_depth':np.arange(1,10)
#'random_state':np.arange(1,100)

}

best_sj_model = train(x,y,estimator,params)



'''
from statsmodels.tsa.stattools import adfuller
def test_stationarity(timeseries):
    
    #Determing rolling statistics
    rolmean = pd.rolling_mean(timeseries, window=12)
    rolstd = pd.rolling_std(timeseries, window=12)

    #Plot rolling statistics:
    orig = plt.plot(timeseries, color='blue',label='Original')
    mean = plt.plot(rolmean, color='red', label='Rolling Mean')
    std = plt.plot(rolstd, color='black', label = 'Rolling Std')
    plt.legend(loc='best')
    plt.title('Rolling Mean & Standard Deviation')
    plt.show(block=False)
    
    
    #Perform Dickey-Fuller test:
    print ('Results of Dickey-Fuller Test:')
    dftest = adfuller(timeseries, autolag='AIC')
    
    
    dfoutput = pd.Series(dftest[0:4], index=['Test Statistic','p-value','#Lags Used','Number of Observations Used'])
    for key,value in dftest[4].items():
        dfoutput['Critical Value (%s)'%key] = value
    print(dfoutput)
  
    
#time series analyis
y = pd.DataFrame(y_sj).astype('float64')

from pylab import rcParams
rcParams['figure.figsize'] = 11, 9
freq = 52
decomposition = sm.tsa.seasonal_decompose(y,freq=freq, model='additive')
fig = decomposition.plot()
plt.show()


pd.options.display.float_format = '{:,.2f}'.format
test_stationarity(y.values[:,0])

moving_avg = pd.rolling_mean(y,12)
y_avg_diff = y - moving_avg
y_avg_diff.dropna(inplace=True)
test_stationarity(y_avg_diff.values[:,0])



expwighted_avg = pd.ewma(y, halflife=12)

plt.plot(y)
plt.plot(expwighted_avg, color='red')

ewma_diff = y - expwighted_avg
test_stationarity(ewma_diff.values[:,0])


trend = decomposition.trend
seasonal = decomposition.seasonal
residual = decomposition.resid

y_decompose = residual
y_decompose.dropna(inplace=True)
test_stationarity(y_decompose.values[:,0])



y_avg_diff = y_avg_diff.values
#ACF and PACF plots:
from statsmodels.tsa.stattools import acf, pacf

lag_acf = acf(y_avg_diff, nlags=20)
lag_pacf = pacf(y_avg_diff, nlags=20, method='ols')

from scipy import stats
stats.boxcox(y.values[:,0])

#Plot ACF: 
plt.subplot(121) 
plt.plot(lag_acf)
plt.axhline(y=0,linestyle='--',color='gray')
plt.axhline(y=-1.96/np.sqrt(len(y_avg_diff)),linestyle='--',color='gray')
plt.axhline(y=1.96/np.sqrt(len(y_avg_diff)),linestyle='--',color='gray')
plt.title('Autocorrelation Function')

#Plot PACF:
plt.subplot(122)
plt.plot(lag_pacf)
plt.axhline(y=0,linestyle='--',color='gray')
plt.axhline(y=-1.96/np.sqrt(len(y_avg_diff)),linestyle='--',color='gray')
plt.axhline(y=1.96/np.sqrt(len(y_avg_diff)),linestyle='--',color='gray')
plt.title('Partial Autocorrelation Function')
plt.tight_layout()

#p = 2  q 8 

model = ARIMA(y.values, order=(2, 1, 0),freq="W")  
results_AR = model.fit(disp=-1)  
plt.plot(y_avg_diff,color='blue')
plt.plot(results_AR.fittedvalues, color='red')
plt.title('RSS: %.4f'% np.sum(((results_AR.fittedvalues-y_avg_diff)**2)))


model = ARIMA(y.values, order=(0, 1, 8))  
results_MA = model.fit(disp=-1)  
plt.plot(y_avg_diff)
plt.plot(results_MA.fittedvalues, color='red')
plt.title('RSS: %.4f'% np.sum((results_MA.fittedvalues-y_avg_diff)**2))


model = ARIMA(y.values, order=(2, 1, 8))  
results_ARIMA = model.fit(disp=-1)  
plt.plot(y_avg_diff)
plt.plot(results_ARIMA.fittedvalues, color='red')
plt.title('RSS: %.4f'% np.sum((results_ARIMA.fittedvalues-y_avg_diff)**2))


print("arima")
# fit model
model = ARIMA(y.values, freq = 'W',
              order=(12,0,2))
model_fit = model.fit()
print(model_fit.summary())
model_fit.plot_predict(1,len(y)+800)


best_sj_model = model_fit
'''


#model_fit.forecast(30)
#model_fit.predict(start=X.shape[0], end=X.shape[0])
# plot residual errors
'''
residuals = pd.DataFrame(model_fit.resid)
residuals.plot()
plt.show()
residuals.plot(kind='kde')
plt.show()
print(residuals.describe())
'''
'''
there is no trend
there is some seasonallity
'''

'''
#plot line plot from predictions 
#and real values
fig = plt.figure(figsize=(7, 5), dpi=100)
y_true = dfs_train[1]['total_cases'].copy().append(dfs_train[0]['total_cases'])
sj_pred = back_transform_sj_y(best_sj_model.predict(x_sj))
iq_pred = back_transform_iq_y(best_iq_model.predict(x_iq))
ticks = np.arange(len(y_true))
y_pred = np.concatenate([sj_pred,iq_pred])
plt.plot(ticks,y_true,c='b',label = "Real Values")
plt.plot(ticks,y_pred,c='r',label = "Predicts")
plt.legend()
plt.show()
'''



'''
#reading dfs
features_path2 = path + "dengue_features_test.csv"
df_test = pd.read_csv(features_path2)
dfs_test,old_len_predictors = pre_process_data(df = df_test)

#predictions
#para cada modelo

predictions = []
for df_test,best_model,process,back_transform in zip(dfs_test,[best_iq_model,best_sj_model],[pre_process_iq,pre_process_sj],[back_transform_iq_y,back_transform_sj_y]):
    df_test = df_test.copy()
    to_predict = process(df_test,get_response=False)
    df_test['total_cases'] = back_transform(best_model.predict(to_predict))
    predictions.append(df_test['total_cases'])
    
    fig = plt.figure(figsize=(26, 10), dpi=100)
    ticks = np.arange(len(df_test))
    plt.plot(ticks,df_test['total_cases'],c='r',label = "Predicts")
    plt.legend()
    plt.show()

#reverse pred -> sj iq   
predictions = predictions[::-1]


#SAVING SUMISSION FILE
submission = pd.read_csv(path + "submission_format.csv",
                         index_col=[0, 1, 2])
result_path = "result/"
submission.total_cases = np.concatenate(predictions)
submission.to_csv(result_path+"result.csv")
'''