sj model selection based on CV and total mae score
model with high difference between those scores were discarted.
see delete.py

year has good association
but the test data has only out of range values,
 so it shouldn't be a good predictor




SVR linear degree 2
     'station_max_temp_c_12',
     'reanalysis_avg_temp_k_10',
     'reanalysis_dew_point_temp_k_9',
     'station_avg_temp_c_10',
     'station_avg_temp_c_12',
     ]
cv error -21.2977669387
full data error -21.0291144439  



SVR rbf degree 2 c  = 6
     'reanalysis_avg_temp_k_10',
     'station_avg_temp_c_10',
     'station_avg_temp_c_12',
     ]
cv error -21.1037970822
full data error -20.4752784701
low overfit (.6 error)


#degree = 2 NuSVR
     'reanalysis_avg_temp_k_10',
     'station_avg_temp_c_10',
     'station_avg_temp_c_12',
estimator = NuSVR(nu=.83,C=10)  
cv error -21.0256188905
full data error -20.4400800124
low overfit (.6 error)

adicionando year (e removendo scallings e poly)
cv error -17.9605278787
full data error -16.4329474483


predictions:

since year ins't a good variable, its removal may improve model performance
also, considering that, the model has lower mae error and its less overfit,
 meaning more stable results
the size if sj dataset should also be considered, since has almost 2x rows more than iq, 
and improvements in this model should be more impactful on the final performance
I predict (approx) an 1 error improvement, but a 2 point error is also possible
          
NuSVR > SVR rbf probably
but I don't know if they'll be better than SVR linear (has chance)

NSVR > SVR rbf > SVR linear

NSVR didn't improve 25.7957
rbf was worse 25.9231
linear 25.7572

the results doesn't match with my predictions
maybe the removal of 'year' hurt the final result?


try adding weekofyear to the models and see if it helps
add year back


model from pip 5
    explanatory = [
    'station_max_temp_c_12',
    'reanalysis_dew_point_temp_k_9',
    'reanalysis_specific_humidity_g_per_kg_9',
    'reanalysis_dew_point_temp_k_8',
    'station_min_temp_c_10',
    'station_max_temp_c_11',
    'reanalysis_specific_humidity_g_per_kg',
    'year',
    'reanalysis_dew_point_temp_k',
    'reanalysis_min_air_temp_k',
    ]
estimator = GradientBoostingRegressor(random_state=100,n_estimators=20)
-25.0670426654 {}
cv error -25.0670426654
full data error -17.972169509
high CV error and the model is clearly overfit
but it perform better than the other models. why????

                                     

try submit this one
base model derived from earlier (pip5)
estimator = NuSVR(nu=.83,C=10)  #<<<< 
cv error -18.4458813296
full data error -15.2889679111


    'reanalysis_dew_point_temp_k_9',
    'reanalysis_specific_humidity_g_per_kg',
    'year',
estimator = NuSVR(nu=.83,C=10)  #<<<< 
cv error -16.688463616
full data error -14.9794094018
29.1034
the model got a lot worse. why????

>I was using custom CV validation in the last models and it showed better values when
compared with the default one

use the default one in those new models and compare results
NuSVR doesnt seem worse      
cv error -21.8854391964
full data error -20.4937949285   
but got worse with year addition
cv error -27.0492226032
full data error -16.4329474483

SVR not worse either
cv error -21.7756125501
full data error -20.5508949297


also nan management should be done for each dataset (iq and sj) after the split, not before
also the shift operation and others data transformations
but it didn't change much that situation.

improve the best model from test data (GBR)

     'reanalysis_avg_temp_k_10',
     'station_avg_temp_c_10',
     'station_avg_temp_c_12',
     ]
estimator = SVR()  26.4543

The plot looks different at the end so Im assuming that the test data requers a very
specific model that (for now) can be blindly optimized for 

                    
                    
                    
                    
                    
model 1  estimator = GradientBoostingRegressor(random_state=100,n_estimators=20)#-25.0726242515
 
                            poly 2
    explanatory = [
 'station_avg_temp_c_10',
 'station_avg_temp_c_9',
 'station_avg_temp_c_11',
 'station_avg_temp_c_8',
 'station_min_temp_c_10',
 'station_avg_temp_c_12',
 'station_min_temp_c_9',
 'station_avg_temp_c_7',
 'station_min_temp_c_11',
 'station_min_temp_c_8',
 'station_min_temp_c_7',
 'station_min_temp_c_12',
 'station_avg_temp_c_6',
 'station_min_temp_c_6',
 'station_avg_temp_c_5',
 #'station_min_temp_c_5',
 'station_max_temp_c_11',
 #'station_max_temp_c_12',
 'station_max_temp_c_10',
 'station_max_temp_c_8',
 #'station_max_temp_c_9',
 #'station_avg_temp_c_4',
 #'reanalysis_min_air_temp_k_8',
 'reanalysis_dew_point_temp_k_8',
 'reanalysis_min_air_temp_k_7',
 #'reanalysis_specific_humidity_g_per_kg_8',
 #'station_max_temp_c_7',
     ]
cv error -24.386329043
full data error -17.8453284184

25.5216
                    
                    
                    
                    



submeter o modelo normal com o xboost

submeter modelo mais simples

depois melhorar com grau, scalling e selecao de parametros 
+ selecao de variaveis + runs


XGBOOST
no scalling
degree 1
max_leafs 2
same variables as sj from pip 5
run 3
2      20.936602      1.095427       20.304512       0.353352

27.7284
         

max depth 2
scalling
degree 2 
run 4
3       21.846655      0.763256       21.437995       0.300186           

really low std
26.1010


run time for
max depth 3
explanatory = [
    'station_max_temp_c_12',
    'reanalysis_specific_humidity_g_per_kg_9',
    'station_min_temp_c_10',
    'reanalysis_specific_humidity_g_per_kg',
    'reanalysis_min_air_temp_k',]

27.0264



    explanatory = [
    'station_max_temp_c_12',
    'reanalysis_specific_humidity_g_per_kg_9',
    'reanalysis_dew_point_temp_k_8',
    'year',
    ]
    no dg no sc
2      22.387322     19.834472       16.524693       1.530716
param = {'max_depth':5}
run 3

28.0000