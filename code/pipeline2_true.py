# -*- coding: utf-8 -*-
"""
Created on Tue May  2 12:36:49 2017

@author: icaromarley5
"""

'''
prestar atencao nos formatos dos dados sctter e hexbins e aplicar transformacoes
modelo prevendo resultados negativos/??//??/?

--usar bagging/boosting
--gridsearchcv
--improve randomstatemodel com fit e refit

ver data transformation.
pipeline sklearn
time series, 
pgm
fill na com valores próxima
escolher um melhor normalizador
''' 

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats
import seaborn as sns
import winsound

#preprocess data and returns 2 dataframes with Iquitos and San Juan Data
def pre_process_data(df,response=None,weeks_before=5): 
    #splitting to model the 2 cities
    df = df.copy()
    columns = ['week_start_date']
    df.drop(columns,axis=1,inplace=True)
    
    df_train_iq = df[df['city'] == 'iq'].copy()
    df_train_sj = df[df['city'] == 'sj'].copy()
    
    
  
    #getting values from past weeks
    dfs_train = [df_train_iq,df_train_sj]
    
    for df_train in dfs_train:
        df_train.drop(['city'],inplace=True,axis=1)
    
    
    columns = df_train_sj.columns.values
    if response != None:
        columns = np.delete(columns,df_train.columns.get_loc(response))

    old_len_columns = len(columns) 
    
    
    for n in range(1,weeks_before+1):
         for df_train in dfs_train:  
             for column in columns:
                 df_train[column+"_"+ str(n)] = df_train[column].shift(n)

    #dealing with na values -> fill with mean
    for df_train in dfs_train:
        df_train.fillna(df_train.mean(),inplace=True)

    if response != None:
        predictors = np.delete(df_train.columns,df_train.columns.get_loc(response))
    else:
        predictors = df_train.columns
    return dfs_train,predictors,old_len_columns


#reading training files
path = "../modified data/"
features_path = path + "dengue_features_train.csv"
labels_path = path + "dengue_labels_train.csv"

df_feat = pd.read_csv(features_path)
df_lab = pd.read_csv(labels_path)

#merge
df_merge = pd.merge(df_feat,df_lab,on=["city","year","weekofyear"])


#selecting response
response = "total_cases"

dfs_train,predictors,old_len_columns = pre_process_data(df_merge,response)


'''
#hist plot
f, axarr = plt.subplots(1,2,sharex=True,sharey=True,figsize=(7,4))
index = 0 
for city,df_train in zip(["Iquitos","San Juan"],dfs_train):
    f.subplots_adjust(hspace=.3)
    axarr[index].hist(df_train[response], 50)
    axarr[index].set_title(city)
    index+=1
plt.suptitle("Histogram of total dengue fever cases")
plt.show()

#line plot
f, axarr = plt.subplots(1,2,sharey=True,figsize=(7,4))
index = 0 
for city,df_train in zip(["Iquitos","San Juan"],dfs_train):
    f.subplots_adjust(hspace=.3)
    axarr[index].plot(df_train[response])
    axarr[index].set_title(city)
    index+=1
plt.suptitle("Line Plot of total dengue fever cases")
plt.show()

#correlations

for city,df_train in zip(["Iquitos","San Juan"],dfs_train):
    corrmat = df_train.corr()
    
    fig = plt.figure(figsize=(26, 10), dpi=100)
    ticks = np.arange(len(corrmat['total_cases']))
    plt.plot(ticks,corrmat['total_cases'])
    labels = corrmat['total_cases'].index.values
    plt.xticks(ticks, labels, rotation='vertical')
    plt.title(city)
    plt.show()


#usar para hexbin tbm
#scatter plot predictor x reponse by medition week 
len_side = 6
predictors_len = len(predictors)
for city,df_train in zip(["Iquitos","San Juan"],dfs_train):
    df_train.dropna(inplace=True)
    step = old_len_columns
    limit_inf = 0
    limit_max = step
    week = 0
    while (limit_max<=predictors_len):
        index = 0
        side = 0
        selected = predictors[limit_inf:limit_max]
        
        f, axarr = plt.subplots(int(len(selected)/len_side) + 1,len_side,
                                figsize=(19,14),sharey=True)
        f.subplots_adjust(hspace=.5)
        for predictor in selected:
            x = df_train[predictor].apply(lambda x: x)
            y = df_train[response]
            #print(predictor,index,side)
            axarr[index,side].scatter(x,y,c='b')
            axarr[index,side].set_xlabel(predictor)
            corr = scipy.stats.pearsonr(x,y)
            axarr[index,side].set_title("Pearson R {:.2f} P {:.2f}".format(corr[0],corr[1]))
            side+=1
            if side > len_side - 1:
                side = 0
                index+=1   
        plt.suptitle(city + ": total dengue fever cases by predictors week " + str(week))
        plt.show()  
        
        #print(limit_inf,limit_max,selected,step)
        limit_inf = limit_max
        limit_max += step
        week+= -1



'''

'''
ANALYSIS

iquitos
few outliers, mais bem comportada
menos dados

as semanas anteriores tem mais relação linear com a variavel!!!
linear
pioram nas semanas anreriores principalmente 3 4 5

poly 2
week of year
reanalysis dew point temp k 
log10 x -> reanalysis min air temp k boa
reanalysis min air temp k 
reanaçysis precip amt kg per m2
reanaçysis relative umidity percent
reanaçysis specific umidity per kg
station avg temp c
station min temp c
station precip mm



san juan, varia muito muitos picos
many outliers 
mais dados

linear
weekofyear 0 1 2 3 4

variaveis da semana 3 4 5 -> 4 5
dew point temp
humidity g per kg
station avg temp c
station min temp c


poly 2
week of year
reanalysis air temp k 
precipitation amt mm
reanalysis avg temp k  -> log1p
reanalysis dew point temp k  -> log10 -> log1p
reanalysis max air temp k  -> log10 -> log1p week>=2
reanalysis min air temp k  -> log1p
station avg temp c
reanaçysis specific umidity per kg
reanaçysis precip amt kg per m2
statin precip mm
reanaçysis sat precip amt mm

other regression   ->>>> power 2 em x   vira poly 2
station_diur_temp_rng_c
ndvi_ne
ndvi_nw
ndvi_se
ndvi_sw
'''

#TRAINING

from sklearn.preprocessing import PolynomialFeatures,StandardScaler
from sklearn.model_selection import GridSearchCV
#train models with gridsearchcv
def train(x,y,estimator,params):
    clf = GridSearchCV(estimator, params,scoring="neg_mean_absolute_error",cv=10)#,verbose=10000000000)
    clf.fit(x,y)
    #print("best score",clf.best_score_)
    #print("best params",clf.best_params_)
    #winsound.Beep(300,300)
    return clf.best_estimator_,clf.best_score_  

def train_final(x,y,best_model):###modificar
    best_model = SVR(**best_model.get_params())
    best_model.fit(x,y)
    return best_model
    


#IQUITOS MODEL
#svr linear  -6.00528143428

from sklearn.svm import SVR
from sklearn import preprocessing




#IQUITOS MODEL  SVR LINEAR -5.6472386594704211 
df_train = dfs_train[0].copy()
explanatory = ['ndvi_nw',
 'ndvi_se',
 'ndvi_sw',
 'reanalysis_dew_point_temp_k',
 'reanalysis_min_air_temp_k',
 'reanalysis_relative_humidity_percent',
 'reanalysis_specific_humidity_g_per_kg',
 'station_avg_temp_c',
 'year_1',
 'ndvi_sw_1',
 'reanalysis_max_air_temp_k_1',
 'reanalysis_relative_humidity_percent_1',
 'reanalysis_tdtr_k_1',
 'weekofyear_2',
 'ndvi_se_2',
 'reanalysis_max_air_temp_k_2',
 'reanalysis_relative_humidity_percent_2',
 'reanalysis_sat_precip_amt_mm_2',
 'reanalysis_specific_humidity_g_per_kg_2',
 'station_avg_temp_c_2',
 'station_diur_temp_rng_c_2',
 'station_max_temp_c_2',
 'station_precip_mm_2',
 'year_3',
 'ndvi_nw_3',
 'ndvi_se_3',
 'precipitation_amt_mm_3',
 'reanalysis_max_air_temp_k_3',
 'reanalysis_min_air_temp_k_3',
 'reanalysis_specific_humidity_g_per_kg_3',
 'reanalysis_tdtr_k_3',
 'station_avg_temp_c_3',
 'station_diur_temp_rng_c_3',
 'weekofyear_4',
 'ndvi_ne_4',
 'ndvi_se_4',
 'reanalysis_avg_temp_k_4',
 'reanalysis_max_air_temp_k_4',
 'reanalysis_min_air_temp_k_4',
 'reanalysis_precip_amt_kg_per_m2_4',
 'reanalysis_sat_precip_amt_mm_4',
 'reanalysis_tdtr_k_4',
 'station_avg_temp_c_4',
 'station_diur_temp_rng_c_4',
 'station_max_temp_c_4',
 'station_min_temp_c_4',
 'station_precip_mm_4',
 'weekofyear_5',
 'ndvi_nw_5',
 'ndvi_se_5',
 'reanalysis_air_temp_k_5',
 'reanalysis_avg_temp_k_5',
 'reanalysis_dew_point_temp_k_5',
 'reanalysis_tdtr_k_5',
 'station_min_temp_c_5']

x = df_train[explanatory].copy()
y = df_train[response].copy()
scaler = preprocessing.StandardScaler().fit(x)
x = scaler.transform(x) 


estimator = SVR(kernel='linear',C=4, epsilon = 0.089999999999999997,tol=0.00092000000000000014)
params = {}
best_iq_model,score = train(x,y,estimator,params)
print(score)

best_iq_model = train_final(x,y,best_iq_model)

explanatory_iq = explanatory




#SAN JUAN  MODEL  SVR LINEAR  -21.317193218701124 
df_train = dfs_train[1].copy()
explanatory = ['precipitation_amt_mm',
 'reanalysis_relative_humidity_percent',
 'station_avg_temp_c',
 'station_diur_temp_rng_c',
 'station_min_temp_c',
 'year_1',
 'ndvi_ne_1',
 'station_avg_temp_c_1',
 'station_diur_temp_rng_c_1',
 'station_max_temp_c_1',
 'station_min_temp_c_1',
 'precipitation_amt_mm_2',
 'reanalysis_air_temp_k_2',
 'reanalysis_avg_temp_k_2',
 'reanalysis_relative_humidity_percent_2',
 'reanalysis_sat_precip_amt_mm_2',
 'reanalysis_specific_humidity_g_per_kg_2',
 'station_avg_temp_c_2',
 'station_max_temp_c_2',
 'station_min_temp_c_2',
 'station_precip_mm_2',
 'ndvi_se_3',
 'reanalysis_dew_point_temp_k_3',
 'reanalysis_max_air_temp_k_3',
 'reanalysis_sat_precip_amt_mm_3',
 'reanalysis_specific_humidity_g_per_kg_3',
 'station_avg_temp_c_3',
 'station_min_temp_c_3',
 'year_4',
 'weekofyear_4',
 'ndvi_se_4',
 'precipitation_amt_mm_4',
 'reanalysis_avg_temp_k_4',
 'reanalysis_relative_humidity_percent_4',
 'reanalysis_specific_humidity_g_per_kg_4',
 'station_avg_temp_c_4',
 'station_diur_temp_rng_c_4',
 'station_max_temp_c_4',
 'station_min_temp_c_4',
 'year_5',
 'weekofyear_5',
 'ndvi_ne_5',
 'ndvi_sw_5',
 'reanalysis_avg_temp_k_5',
 'reanalysis_max_air_temp_k_5',
 'reanalysis_relative_humidity_percent_5',
 'reanalysis_specific_humidity_g_per_kg_5',
 'station_avg_temp_c_5',
 'station_diur_temp_rng_c_5',
 'station_min_temp_c_5',
 'station_precip_mm_5']
x = df_train[explanatory]
y = df_train[response]
scaler = StandardScaler().fit(x)
x = scaler.transform(x)  
estimator = SVR(kernel="linear",epsilon=0.076999999999999999,C=10.900000000000009)
params = {}
best_sj_model,score = train(x,y,estimator,params)
print(score)

best_sj_model = train_final(x,y,best_sj_model)

explanatory_sj = explanatory


"""
#feature selection (backwards elimination)  svr linear
best_score = -5.6472386594704211 
explanatory = list(predictors)
old_len = 0
new_len = len(explanatory)
while (old_len != new_len):
    print("TENTANDO NOVAMENTE")
    i = 0
    while (i<len(explanatory)):
        old_len = new_len
        print(i)
        selected = explanatory.pop(i)
     
        x = df_train[explanatory].copy()
        y = df_train[response].copy()
        scaler = preprocessing.StandardScaler().fit(x)
        x = scaler.transform(x) 
        estimator = SVR(kernel='linear',C=4, epsilon = 0.089999999999999997,tol=0.00092000000000000014)
        params = {}
        best_iq_model,score = train(x,y,estimator,params)
        if score > best_score:
            best_score = score
            print("best score so far",best_score,"len",len(explanatory))
        else:
            explanatory.insert(i,selected)
            i+=1
    
    new_len = len(explanatory)
  
""" 


#reading dfs
features_path2 = path + "dengue_features_test.csv"
df_test = pd.read_csv(features_path2)
dfs_test,predictors,old_len_predictors = pre_process_data(df = df_test)

#predictions
#para cada modelo

predictions = []
for df_test,best_model,x in zip(dfs_test,[best_iq_model,best_sj_model],[explanatory_iq,explanatory_sj]):
    to_predict = df_test[x]
    scaler = preprocessing.StandardScaler().fit(to_predict)
    to_predict = scaler.transform(to_predict) 
    df_test['total_cases'] = best_model.predict(to_predict)
    predictions.append(df_test['total_cases'].abs().astype(int))

#SAVING SUMISSION FILE
submission = pd.read_csv(path + "submission_format.csv",
                         index_col=[0, 1, 2])
result_path = "result/"
submission.total_cases = np.concatenate(predictions)
submission.to_csv(result_path+"result.csv")




#saving
#para cada modelo
#from sklearn.externals import joblib
#model_path = "models/"
#joblib.dump(estimator, model_path+ 'SECONDSUB_RFR.pkl') 
#clf = joblib.load('filename.pkl')

