# -*- coding: utf-8 -*-
"""
Created on Sun May 14 02:09:44 2017

@author: silvia-pc
"""

from sklearn.svm import NuSVR,LinearSVR
from sklearn.ensemble import AdaBoostRegressor,BaggingRegressor,GradientBoostingRegressor

"""
variables1= ['station_avg_temp_c_12',
 'station_min_temp_c_10',
 'reanalysis_specific_humidity_g_per_kg_9',
 'reanalysis_dew_point_temp_k_9',
 'station_max_temp_c_12',
 'reanalysis_min_air_temp_k_9',
 'reanalysis_max_air_temp_k_9',
 'reanalysis_air_temp_k_9']
    

variables2 = ['station_avg_temp_c_10',
 'station_min_temp_c_10',
 'station_max_temp_c_11',
 'reanalysis_min_air_temp_k_8',
 'reanalysis_dew_point_temp_k_8',
 'reanalysis_specific_humidity_g_per_kg_8',
 'reanalysis_air_temp_k_8',
 'reanalysis_max_air_temp_k_7',
 'reanalysis_avg_temp_k_8',
 'reanalysis_max_air_temp_k_8']


variables3 =['station_avg_temp_c_12',
 'station_min_temp_c_10',
 'station_max_temp_c_12',
 'reanalysis_specific_humidity_g_per_kg_8',
 'reanalysis_dew_point_temp_k_9',
 'reanalysis_min_air_temp_k_9',
 'reanalysis_max_air_temp_k_10',
 'reanalysis_air_temp_k_9',
 'reanalysis_avg_temp_k_10']


variables_all = ['station_max_temp_c_11',
 'station_max_temp_c_12',
 'reanalysis_max_air_temp_k_9',
 'reanalysis_max_air_temp_k_7',
 'reanalysis_avg_temp_k_10',
 'reanalysis_air_temp_k_9',
 'reanalysis_min_air_temp_k_8',
 'reanalysis_air_temp_k_8',
 'reanalysis_dew_point_temp_k_8',
 'reanalysis_specific_humidity_g_per_kg_9',
 'reanalysis_dew_point_temp_k_9',
 'reanalysis_max_air_temp_k_10',
 'reanalysis_min_air_temp_k_9',
 'station_min_temp_c_10',
 'reanalysis_specific_humidity_g_per_kg_8',
 'station_avg_temp_c_10',
 'reanalysis_avg_temp_k_8',
 'station_avg_temp_c_12',
 'reanalysis_max_air_temp_k_8']
"""


'''
SAN JUAN  
'''
print('\nSAN JUAN MODEL TRAINING\n')
df_train = dfs_train[1]
def back_transform_sj_y(values):
    return values.round(0).astype(int)

#return x
def pre_process_sj(df,get_response=True):
    explanatory = [
 'station_avg_temp_c_10',
 'station_avg_temp_c_9',
 'station_avg_temp_c_11',
 'station_avg_temp_c_8',
 'station_min_temp_c_10',
 'station_avg_temp_c_12',
 'station_min_temp_c_9',
 'station_avg_temp_c_7',
 'station_min_temp_c_11',
 'station_min_temp_c_8',
 'station_min_temp_c_7',
 'station_min_temp_c_12',
 'station_avg_temp_c_6',
 'station_min_temp_c_6',
 'station_avg_temp_c_5',
 #'station_min_temp_c_5',
 'station_max_temp_c_11',
 #'station_max_temp_c_12',
 'station_max_temp_c_10',
 'station_max_temp_c_8',
 #'station_max_temp_c_9',
 #'station_avg_temp_c_4',
 #'reanalysis_min_air_temp_k_8',
 'reanalysis_dew_point_temp_k_8',
 'reanalysis_min_air_temp_k_7',
 #'reanalysis_specific_humidity_g_per_kg_8',
 #'station_max_temp_c_7',
     ]
    
    
    x = df[explanatory].copy()
    
    scaler = preprocessing.StandardScaler().fit(x)
    x = scaler.transform(x) 
    poly = PolynomialFeatures(degree=2)
    x = poly.fit_transform(x)
    
    if get_response:
        y = df[response].copy()
        return x,y
    return x


x_sj,y_sj = pre_process_sj(df_train)
x = x_sj
y = y_sj


#"""

#estimator = SVR(kernel='linear')#nice     
#estimator = SVR(C=6)#nice  

#estimator = NuSVR(nu=.83,C=10)  
               
#estimator = LinearSVR(random_state=100,max_iter=4000)

estimator = GradientBoostingRegressor(random_state=100,n_estimators=20) #overfitting
#estimator = NuSVR(nu=.83,C=10)  
#estimator = AdaBoostRegressor(estimator,random_state=100,n_estimators=500)

'''
estimator = RandomForestRegressor(random_state=100,n_jobs = -1,n_estimators=i) #ovefitting or underfitting
estimator = GradientBoostingRegressor(random_state=100) #overfitting



estimator = linear_model.LinearRegression()  #high bias
estimator = GradientBoostingRegressor(random_state=100) #overfitting
'''


params = {
#"n_estimators":np.append(np.arange(21,101),20)
}

# Non_nested parameter search and scoring
best_sj_model,best_params,score = train(x,y,estimator,params)
#print("inside CV score",score,best_params)

#full data score
pred = best_sj_model.predict(x)
true = df_train[response]
full_score = - mean_absolute_error(true,pred)
#print("full dataset score",full_score)

#print('difference',abs(full_score - score))
print("cv error",score)
print("full data error",full_score)
print("best params",best_params)
#"""





"""
i_min = 1
i_max = 20
scores_cv =  []
scores_full = []  
i_range = np.arange(i_min,i_max)
i = i_min
for i in i_range:
    estimator = SVR(C=i)#nice  
    '''
    estimator = GradientBoostingRegressor(random_state=100,n_estimators=i) #overfitting
   
    estimator = RandomForestRegressor(random_state=100,n_jobs = -1,n_estimators=i) #ovefitting or underfitting

    estimator = SVR(C=i)#nice
    
    estimator = linear_model.LinearRegression()  #high bias
    estimator = GradientBoostingRegressor(random_state=100) #overfitting
    '''
    
    
    params = {
    
    }
    
    
    
    # Choose cross-validation techniques for the inner and outer loops,
    # independently of the dataset.
    # E.g "LabelKFold", "LeaveOneOut", "LeaveOneLabelOut", etc.
    inner_cv = KFold(n_splits=10, shuffle=True, random_state=100)
    outer_cv = KFold(n_splits=4, shuffle=True, random_state=100)
    
    # Non_nested parameter search and scoring
    best_sj_model,best_params,score = train(x,y,estimator,params,inner_cv)
    #print("inside CV score",score,best_params)
    
    #full data score
    pred = best_sj_model.predict(x)
    true = df_train[response]
    full_score = - mean_absolute_error(true,pred)
    #print("full dataset score",full_score)
    
    #print('difference',abs(full_score - score))
    scores_cv.append(-score)
    scores_full.append(-full_score)



fig = plt.figure(figsize=(7, 5), dpi=100)
ticks = i_range
plt.plot(ticks,scores_cv,c='b',label = "CV score")
plt.plot(ticks,scores_full,c='r',label = "Full data set score")
plt.xticks(ticks,ticks)
plt.legend()
plt.title("MAE through tests")
plt.show()
"""

#"""
#plotando valores reais contra previsão
fig = plt.figure(figsize=(7, 5), dpi=100)
y_true = dfs_train[1]['total_cases'].copy().append(dfs_train[0]['total_cases'])
sj_pred = back_transform_sj_y(best_sj_model.predict(x_sj))
iq_pred = back_transform_iq_y(best_iq_model.predict(x_iq))
ticks = np.arange(len(y_true))
y_pred = np.concatenate([sj_pred,iq_pred])
plt.plot(ticks,y_true,c='b',label = "Real Values")
plt.plot(ticks,y_pred,c='r',label = "Predicts")
plt.legend()
plt.show()
#"""
