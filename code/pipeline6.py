# -*- coding: utf-8 -*-
"""
Created on Sun May 14 00:49:55 2017

@author: icaromarley5
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats
import seaborn as sns
import winsound
from sklearn.externals import joblib
from sklearn.ensemble import RandomForestRegressor, ExtraTreesRegressor, GradientBoostingRegressor
from sklearn import preprocessing
from sklearn.preprocessing import PolynomialFeatures
from sklearn import linear_model
from sklearn.preprocessing import PolynomialFeatures,StandardScaler
from sklearn.model_selection import GridSearchCV, cross_val_score, KFold
from sklearn.metrics import mean_absolute_error
from sklearn.svm import SVR
from sklearn.svm import NuSVR,LinearSVR
from sklearn.ensemble import AdaBoostRegressor,BaggingRegressor
import xgboost as xgb


#np.set_printoptions(threshold='nan')
#pd.set_option('display.max_rows', None)
#preprocess data and returns 1 df
np.set_printoptions(threshold=np.nan)
def pre_process_data(df,response=None,weeks_before=12): 
    #splitting to model the 2 cities
    df = df.copy()

    columns = ['week_start_date']
    df['week_start_date'] = pd.to_datetime(df['week_start_date'])
    df.set_index('week_start_date',inplace=True)
    #df.drop(columns,axis=1,inplace=True)
    

    
    columns = df.columns.values
    if response != None:
        columns = np.delete(columns,df.columns.get_loc(response))
    columns = np.delete(columns,df.columns.get_loc('city'))
        
    #print(columns)
    old_len_columns = len(columns) 

    #split the dfs
    df_train_iq = df[df['city'] == 'iq'].copy()
    df_train_sj = df[df['city'] == 'sj'].copy()    
    df_train_iq.drop('city',axis=1,inplace=True)
    df_train_sj.drop('city',axis=1,inplace=True)    
    
    dfs_train = [df_train_iq,df_train_sj]
    
    for df in dfs_train:
        #dealing with na values 
        df.fillna(method='ffill',inplace=True) 
        
        for n in range(1,weeks_before+1):
            for column in columns:
                df[column+"_"+ str(n)] = df[column].shift(n)
    
        #dealing with na values for the shift operation
        df.fillna(method='bfill',inplace=True)#columns shifted 
    
        

        
    return dfs_train,old_len_columns



#reading training files
path = "../modified data/"
features_path = path + "dengue_features_train.csv"
labels_path = path + "dengue_labels_train.csv"

df_feat = pd.read_csv(features_path)
df_lab = pd.read_csv(labels_path)

#merge
df_merge = pd.merge(df_feat,df_lab,on=["city","year","weekofyear"])


#selecting response
response = "total_cases"

dfs_train,old_len_columns = pre_process_data(df_merge,response)


'''
#hist plot
f, axarr = plt.subplots(1,2,sharex=True,sharey=True,figsize=(12,4))
index = 0 
for city,df_train in zip(["Iquitos","San Juan"],dfs_train):
    f.subplots_adjust(hspace=.3)
    axarr[index].hist(df_train[response], 50)
    axarr[index].set_title(city)
    index+=1
plt.suptitle("Total dengue fever cases")
plt.show()


#line plot
f, axarr = plt.subplots(1,2,sharey=True,sharex=True,figsize=(12,4))
index = 0 
for city,df_train in zip(["Iquitos","San Juan"],dfs_train):
    #df_train.set_index(['weekofyear','year'],inplace=True)
    f.subplots_adjust(hspace=.3)
    axarr[index].plot(df_train[response])
    if city == "Iquitos":
        axarr[index].set_ylabel("Total dengue fever cases")
    axarr[index].set_title(city)
    axarr[index].set_xlabel("Year")
    index+=1
    #df_train.reset_index()
plt.show()

#correlations
for city,df_train in zip(["Iquitos","San Juan"],dfs_train):
    corrmat = df_train.corr()
    fig = plt.figure(figsize=(50, 10), dpi=100)
    ticks = np.arange(len(corrmat['total_cases']))
    plt.plot(ticks,corrmat['total_cases'])
    labels = corrmat['total_cases'].index.values
    plt.xticks(ticks, labels, rotation='vertical')
    plt.title(city)
    plt.show()

for method in ['pearson','spearman','kendall']:
    print(method.upper())
    for df_train,name in zip(dfs_train,['IQUITOS','SAN JUAN']):
        print(name)
        corr = df_train.corr(method=method)['total_cases']
        corr.drop('total_cases',inplace=True)
        print(corr.sort_values(ascending = False)[:50])
    print()

     


PEARSON
SAN JUAN
station_avg_temp_c_10                       0.372402
station_avg_temp_c_9                        0.370052
station_avg_temp_c_11                       0.368241
station_avg_temp_c_8                        0.366818
station_min_temp_c_10                       0.365743
station_avg_temp_c_12                       0.362627
station_min_temp_c_9                        0.361975
station_avg_temp_c_7                        0.358751
station_min_temp_c_11                       0.357216
station_min_temp_c_8                        0.355653
station_min_temp_c_7                        0.349232
station_min_temp_c_12                       0.347039
station_avg_temp_c_6                        0.345253
station_min_temp_c_6                        0.338836
station_avg_temp_c_5                        0.330471
station_min_temp_c_5                        0.322725
station_max_temp_c_11                       0.320711
station_max_temp_c_12                       0.320619
station_max_temp_c_10                       0.315794
station_max_temp_c_8                        0.313152
station_max_temp_c_9                        0.310728
station_avg_temp_c_4                        0.306721
reanalysis_min_air_temp_k_8                 0.306086
reanalysis_dew_point_temp_k_8               0.305654
reanalysis_min_air_temp_k_7                 0.305520
reanalysis_specific_humidity_g_per_kg_8     0.305335
station_max_temp_c_7                        0.303927
reanalysis_dew_point_temp_k_9               0.302149
reanalysis_min_air_temp_k_9                 0.301833
reanalysis_air_temp_k_8                     0.301337
reanalysis_specific_humidity_g_per_kg_7     0.301323
reanalysis_specific_humidity_g_per_kg_9     0.300892
reanalysis_dew_point_temp_k_7               0.300795
reanalysis_air_temp_k_7                     0.299176
reanalysis_max_air_temp_k_7                 0.299044
station_max_temp_c_6                        0.298730
station_min_temp_c_4                        0.298615
reanalysis_min_air_temp_k_6                 0.298415
reanalysis_avg_temp_k_8                     0.298234
reanalysis_max_air_temp_k_8                 0.298132
reanalysis_specific_humidity_g_per_kg_6     0.297880
reanalysis_dew_point_temp_k_6               0.296784
reanalysis_air_temp_k_9                     0.295266
reanalysis_avg_temp_k_7                     0.294937
reanalysis_dew_point_temp_k_10              0.294216
reanalysis_max_air_temp_k_6                 0.293089
reanalysis_avg_temp_k_9                     0.293041
reanalysis_air_temp_k_6                     0.292469
reanalysis_specific_humidity_g_per_kg_10    0.292424
reanalysis_min_air_temp_k_10                0.291679
Name: total_cases, dtype: float64

SPEARMAN
SAN JUAN
station_avg_temp_c_12                       0.575803
station_avg_temp_c_11                       0.567356
station_avg_temp_c_10                       0.564178
station_avg_temp_c_9                        0.552791
station_min_temp_c_10                       0.537087
station_min_temp_c_11                       0.535890
station_min_temp_c_9                        0.531930
station_avg_temp_c_8                        0.531388
station_min_temp_c_12                       0.531031
reanalysis_specific_humidity_g_per_kg_9     0.529533
reanalysis_dew_point_temp_k_9               0.529306
reanalysis_specific_humidity_g_per_kg_8     0.528602
reanalysis_dew_point_temp_k_8               0.527778
reanalysis_dew_point_temp_k_10              0.521208
reanalysis_specific_humidity_g_per_kg_10    0.521064
station_max_temp_c_12                       0.516217
reanalysis_specific_humidity_g_per_kg_7     0.513173
reanalysis_dew_point_temp_k_7               0.511676
station_min_temp_c_8                        0.511587
station_avg_temp_c_7                        0.506964
reanalysis_dew_point_temp_k_11              0.505503
reanalysis_specific_humidity_g_per_kg_11    0.504772
station_max_temp_c_11                       0.497495
reanalysis_dew_point_temp_k_12              0.495754
station_min_temp_c_7                        0.495408
reanalysis_specific_humidity_g_per_kg_12    0.494413
reanalysis_specific_humidity_g_per_kg_6     0.491489
reanalysis_dew_point_temp_k_6               0.489714
reanalysis_min_air_temp_k_9                 0.488150
station_max_temp_c_10                       0.483627
reanalysis_min_air_temp_k_10                0.479006
reanalysis_min_air_temp_k_8                 0.476749
reanalysis_max_air_temp_k_10                0.475994
reanalysis_max_air_temp_k_9                 0.474483
reanalysis_air_temp_k_9                     0.474213
reanalysis_specific_humidity_g_per_kg_5     0.472399
reanalysis_min_air_temp_k_11                0.470750
reanalysis_dew_point_temp_k_5               0.470150
reanalysis_air_temp_k_10                    0.470013
reanalysis_max_air_temp_k_8                 0.469946
reanalysis_min_air_temp_k_7                 0.469923
reanalysis_avg_temp_k_9                     0.467939
reanalysis_air_temp_k_8                     0.466079
station_max_temp_c_9                        0.466035
station_avg_temp_c_6                        0.465321
reanalysis_avg_temp_k_10                    0.465162
reanalysis_max_air_temp_k_11                0.464460
reanalysis_max_air_temp_k_12                0.463813
station_min_temp_c_6                        0.463329
reanalysis_avg_temp_k_8                     0.459454
Name: total_cases, dtype: float64

KENDALL
SAN JUAN
station_avg_temp_c_12                       0.408434
station_avg_temp_c_11                       0.401314
station_avg_temp_c_10                       0.396828
station_min_temp_c_10                       0.392415
station_min_temp_c_11                       0.392287
station_min_temp_c_9                        0.388558
station_avg_temp_c_9                        0.387776
station_min_temp_c_12                       0.387102
station_max_temp_c_12                       0.374053
station_min_temp_c_8                        0.373427
station_avg_temp_c_8                        0.372725
reanalysis_specific_humidity_g_per_kg_8     0.363999
reanalysis_dew_point_temp_k_9               0.363942
reanalysis_specific_humidity_g_per_kg_9     0.363853
reanalysis_dew_point_temp_k_8               0.363781
station_min_temp_c_7                        0.360350
station_max_temp_c_11                       0.358598
reanalysis_dew_point_temp_k_10              0.356909
reanalysis_specific_humidity_g_per_kg_10    0.356465
station_avg_temp_c_7                        0.354297
reanalysis_specific_humidity_g_per_kg_7     0.353308
reanalysis_dew_point_temp_k_7               0.352520
station_max_temp_c_10                       0.347593
reanalysis_dew_point_temp_k_11              0.344434
reanalysis_specific_humidity_g_per_kg_11    0.343382
reanalysis_min_air_temp_k_9                 0.339358
reanalysis_specific_humidity_g_per_kg_6     0.336844
reanalysis_dew_point_temp_k_12              0.335902
reanalysis_dew_point_temp_k_6               0.335802
station_max_temp_c_9                        0.334506
reanalysis_specific_humidity_g_per_kg_12    0.334469
station_min_temp_c_6                        0.334422
reanalysis_min_air_temp_k_10                0.331561
reanalysis_min_air_temp_k_8                 0.331082
reanalysis_max_air_temp_k_10                0.329345
reanalysis_max_air_temp_k_9                 0.328674
reanalysis_min_air_temp_k_7                 0.326966
reanalysis_max_air_temp_k_8                 0.326209
reanalysis_air_temp_k_9                     0.325875
reanalysis_min_air_temp_k_11                0.325135
station_max_temp_c_8                        0.322988
reanalysis_air_temp_k_10                    0.322908
reanalysis_specific_humidity_g_per_kg_5     0.322447
station_avg_temp_c_6                        0.322195
reanalysis_avg_temp_k_9                     0.321984
reanalysis_max_air_temp_k_11                0.321768
reanalysis_max_air_temp_k_12                0.321721
reanalysis_dew_point_temp_k_5               0.321189
reanalysis_air_temp_k_8                     0.320202
reanalysis_avg_temp_k_10                    0.319749
Name: total_cases, dtype: float64


















pearson analysis (linear correlation)
iq model
weekofyear_10                              0.280907 bad
reanalysis_specific_humidity_g_per_kg      0.234918 bad
reanalysis_dew_point_temp_k                0.228864 bad

sj model
station_avg_temp_c_10    0.372402 meh
station_min_temp_c_10    0.365743 bad
station_max_temp_c_11    0.320711 meh
reanalysis_min_air_temp_k_8                 0.306086 ok
reanalysis_dew_point_temp_k_8               0.305654 meh
reanalysis_specific_humidity_g_per_kg_8     0.305335 meh
reanalysis_air_temp_k_8                     0.301337 meh


spearman (monotonic read more)
iq
reanalysis_specific_humidity_g_per_kg      0.369693 bad
reanalysis_dew_point_temp_k                0.365853 bad
year                                       0.329163 bad
reanalysis_min_air_temp_k                  0.311049 bad

sj
station_avg_temp_c_12                       0.575803 meh
station_min_temp_c_10                       0.537087 bad
reanalysis_specific_humidity_g_per_kg_9     0.529533 meh
reanalysis_dew_point_temp_k_9               0.529306 meh
station_max_temp_c_12                       0.516217 meh


kendall
iq

sj
station_avg_temp_c_12                       0.408434 meh
station_min_temp_c_10                       0.392415 bad
station_max_temp_c_12                       0.374053 meh
reanalysis_specific_humidity_g_per_kg_8     0.363999 meh
reanalysis_dew_point_temp_k_9               0.363942 meh








#usar para hexbin tbm
#scatter plot predictor x reponse by medition week 
len_side = 6
predictors = np.delete(df_train.columns,df_train.columns.get_loc(response))
predictors_len = len(predictors)

for city,df_train in zip([["Iquitos","San Juan"][1]],[dfs_train[1]]):
    df_train = df_train.copy()
    step = old_len_columns
    limit_inf = 0
    limit_max = step
    week = 0
    while (limit_max<=predictors_len):
        index = 0
        side = 0
        selected = predictors[limit_inf:limit_max]
        
        f, axarr = plt.subplots(int(len(selected)/len_side) + 1,len_side,
                                figsize=(19,14),sharey=True)
        f.subplots_adjust(hspace=.5)
        for predictor in selected:
            x = df_train[predictor]#.apply(lambda x:np.log(x))
            y = df_train[response]#.apply(lambda x: np.power(x,1/4))
            df_train[predictor] = x
            df_train[response] = y
            #print(predictor,index,side)
            axarr[index,side].scatter(x,y)#,cmap='inferno')
            axarr[index,side].set_xlabel(predictor)
            corr = scipy.stats.pearsonr(x,y)
            axarr[index,side].set_title("Pearson R {:.2f} P {:.2f}".format(corr[0],corr[1]))
            side+=1
            if side > len_side - 1:
                side = 0
                index+=1   
        plt.suptitle(city + ": total dengue fever cases by predictors week " + str(week))
        plt.show()  
        
        #print(limit_inf,limit_max,selected,step)
        limit_inf = limit_max
        limit_max += step
        week+= -1
        
    for method in ['pearson','spearman','kendall']:
        print(method.upper())
        print(city)
        corr = df_train.corr(method=method)['total_cases']
        corr.drop('total_cases',inplace=True)
        print(corr.sort_values(ascending = False)[:20])
        print()
        
'''


#TRAINING
def train(x,y,estimator,params):
    outer_cv = KFold(n_splits=10, shuffle=False)
    inner_cv = KFold(n_splits=5, shuffle=False)
    
    #non nested
    clf = GridSearchCV(estimator, params,scoring="neg_mean_absolute_error",cv=inner_cv)#,verbose=10000000000)
    clf.fit(x,y)
    attributes = [clf.best_estimator_,clf.best_params_,clf.best_score_]
    #nested
    clf = GridSearchCV(estimator, params,scoring="neg_mean_absolute_error",cv=inner_cv)
    nested_score = cross_val_score(clf, X=x, y=y, cv=outer_cv).mean()
    #print(clf.best_params_)
    #winsound.Beep(300,300)
    return attributes + [nested_score]


'''
IQUITOS 

'''

print('\nIQUITOS MODEL TRAINING\n')
df_train = dfs_train[0]
def back_transform_iq_y(values):
    return values.round(0).astype(int)


#return x
def pre_process_iq(df,get_response=True):
    explanatory = [
    'reanalysis_specific_humidity_g_per_kg_3',
    'weekofyear_10',
    ]
    
    x = df[explanatory].copy()
    #scaler = preprocessing.StandardScaler().fit(x)
    #x = scaler.transform(x) 
    #poly = PolynomialFeatures(degree=3)
    #x = poly.fit_transform(x)
    
    if get_response:
        y = df[response].copy()
        return x,y

    return x


x_iq,y_iq = pre_process_iq(df_train)
x = x_iq
y = y_iq

'''
estimator = RandomForestRegressor(random_state=100,n_jobs = -1)
#-6.75011223048 on reanalysis_min_air_temp_k well fitted 
'''

'''
estimator = linear_model.LinearRegression()
#-6.09716923523     'weekofyear_11','weekofyear_12','weekofyear_8','reanalysis_specific_humidity_g_per_kg_3','reanalysis_dew_point_temp_k',
'''

'''
estimator = GradientBoostingRegressor(random_state=100, n_estimators=12)
#-6.18784649128 reanalysis_min_air_temp_k weekofyear_10 (lower overfitting)
'''


estimator = SVR()
#-5.60065916859  on weekofyear_10 reanalysis_specific_humidity_g_per_kg_3 kernel rbf


params = {

}



# Choose cross-validation techniques for the inner and outer loops,
# independently of the dataset.
# E.g "LabelKFold", "LeaveOneOut", "LeaveOneLabelOut", etc.


# Non_nested parameter search and scoring
best_iq_model,best_params,score,nested_score = train(x,y,estimator,params)
print('results')
print('best params',best_params)
print("non nested CV score",score)
print("nested CV score",nested_score)

#full data score
pred = best_iq_model.predict(x)
true = df_train[response]
print("full dataset score",- mean_absolute_error(true,pred))

'''
SAN JUAN 
'''



print('\nSAN JUAN MODEL TRAINING\n')
df_train = dfs_train[1]
def back_transform_sj_y(values):
    return values.round(0).astype(int)

#return x
def pre_process_sj(df,get_response=True):
    explanatory = [
 'station_avg_temp_c_10',
 'station_avg_temp_c_9',
 'station_avg_temp_c_11',
 'station_avg_temp_c_8',
 'station_min_temp_c_10',
 'station_avg_temp_c_12',
 'station_min_temp_c_9',
 'station_avg_temp_c_7',
 'station_min_temp_c_11',
 'station_min_temp_c_8',
 'station_min_temp_c_7',
 'station_min_temp_c_12',
 'station_avg_temp_c_6',
 'station_min_temp_c_6',
 'station_avg_temp_c_5',
 'station_max_temp_c_11',
 'station_max_temp_c_10',
 'station_max_temp_c_8',
 'reanalysis_dew_point_temp_k_8',
 'reanalysis_min_air_temp_k_7',

     ]
    explanatory = [
    'station_max_temp_c_12',
    'reanalysis_specific_humidity_g_per_kg_9',
    'reanalysis_dew_point_temp_k_8',
    'year',
    ]
    x = df[explanatory].copy()

    '''
    scaler = preprocessing.StandardScaler().fit(x)
    x = scaler.transform(x) 
    poly = PolynomialFeatures(degree=1)
    x = poly.fit_transform(x)
    '''
    if get_response:
        y = df[response].copy()
        return x,y
    return x

x_sj,y_sj = pre_process_sj(df_train)
x = x_sj
y = y_sj


print("XGBOOST")

xgtrain = xgb.DMatrix(x, y)
xgtest = xgb.DMatrix(x)

num_round = 3#3
param = {'max_depth':5}
param['nthread'] = 1
param['eval_metric'] = 'mae'

plst = param.items()



bst = xgb.train(plst, xgtrain, num_round)

#full data score
best_sj_model = bst
pred = best_sj_model.predict(xgtest)
true = df_train[response]


plst = param
res = xgb.cv(plst, xgtrain, num_round, nfold=10,
       metrics='mae', shuffle = False)




print("non nested CV score \n",res)
print("full dataset score",- mean_absolute_error(true,pred))

#xgb.plot_importance(bst)

#xgb.plot_tree(bst, num_trees=2)

#plotando valores reais contra previsão
fig = plt.figure(figsize=(7, 5), dpi=100)
y_true = dfs_train[1]['total_cases'].copy().append(dfs_train[0]['total_cases'])
sj_pred = back_transform_sj_y(best_sj_model.predict(xgtest))
iq_pred = back_transform_iq_y(best_iq_model.predict(x_iq))
ticks = np.arange(len(y_true))
y_pred = np.concatenate([sj_pred,iq_pred])
plt.plot(ticks,y_true,c='b',label = "Real Values")
plt.plot(ticks,y_pred,c='r',label = "Predicts")
plt.legend()
plt.show()


'''
#reading dfs
features_path2 = path + "dengue_features_test.csv"
df_test = pd.read_csv(features_path2)
dfs_test,old_len_predictors = pre_process_data(df = df_test)

#predictions
#para cada modelo

first_done = False
predictions = []
for df_test,best_model,process,back_transform in zip(dfs_test,[best_iq_model,best_sj_model],[pre_process_iq,pre_process_sj],[back_transform_iq_y,back_transform_sj_y]):
    df_test = df_test.copy()
    to_predict = process(df_test,get_response=False)
    
    if first_done:
        to_predict = xgb.DMatrix(to_predict)
        
    
    df_test['total_cases'] = back_transform(best_model.predict(to_predict))
    predictions.append(df_test['total_cases'])
    
    fig = plt.figure(figsize=(26, 10), dpi=100)
    ticks = np.arange(len(df_test))
    plt.plot(ticks,df_test['total_cases'],c='r',label = "Predicts")
    plt.legend()
    plt.show()
    first_done = True

#reverse pred -> sj iq   
predictions = predictions[::-1]


#SAVING SUMISSION FILE
submission = pd.read_csv(path + "submission_format.csv",
                         index_col=[0, 1, 2])
result_path = "result/"
submission.total_cases = np.concatenate(predictions)
submission.to_csv(result_path+"result.csv")
'''


'''
#"""
estimator = GradientBoostingRegressor(random_state=100,n_estimators=20)#-25.0726242515

estimator = SVR(kernel='rbf')#nice   
#estimator = SVR(kernel='linear')#nice     
'''

#estimator = NuSVR() 
               
#estimator = LinearSVR(random_state=100,max_iter=4000)


#estimator = SVR(kernel='linear')  
#estimator = AdaBoostRegressor(estimator,random_state=100,n_estimators=500)

'''
estimator = RandomForestRegressor(random_state=100,n_jobs = -1,n_estimators=i) #ovefitting or underfitting
estimator = GradientBoostingRegressor(random_state=100) #overfitting



estimator = linear_model.LinearRegression()  #high bias
estimator = GradientBoostingRegressor(random_state=100) #overfitting
'''

'''
params = {
#'n_estimators':np.arange(1,300)
}

best_sj_model,best_params,score,nested_score = train(x,y,estimator,params)
print('results')
print('best params',best_params)
print("non nested CV score",score)
print("nested CV score",nested_score)

#full data score
pred = best_sj_model.predict(x)
true = df_train[response]
print("full dataset score",- mean_absolute_error(true,pred))
'''


#"""




'''

df_train = dfs_train[0]

#feature selection (backwards elimination) rfr
best_score = -17.6616619913
x,y = pre_process(df_train)
explanatory = list(x.columns)
old_len = 0
new_len = len(explanatory)
while (old_len != new_len):
    print("TENTANDO NOVAMENTE")
    i = 0
    while (i<len(explanatory)):
        old_len = new_len
        print(i)
        selected = explanatory.pop(i)
        x_local = x[explanatory]

        estimator = estimator = RandomForestRegressor(
        random_state= 100,
        #criterion='mae'
        )
        params = {}
        best__model,score = train(x,y,estimator,params)
        if score >= best_score:
            best_score = score
            print("best score so far",best_score,"len",len(explanatory))
        else:
            explanatory.insert(i,selected)
            i+=1
    
    new_len = len(explanatory)
  

print(best_score)
print(explanatory)
winsound.Beep(300,300)

'''




















'''
#plot line plot from predictions 
#and real values

#plotando valores reais contra previsão
fig = plt.figure(figsize=(7, 5), dpi=100)
y_true = dfs_train[1]['total_cases'].copy().append(dfs_train[0]['total_cases'])
sj_pred = back_transform_sj_y(best_sj_model.predict(x_sj))
iq_pred = back_transform_iq_y(best_iq_model.predict(x_iq))
ticks = np.arange(len(y_true))
y_pred = np.concatenate([sj_pred,iq_pred])
plt.plot(ticks,y_true,c='b',label = "Real Values")
plt.plot(ticks,y_pred,c='r',label = "Predicts")
plt.legend()
plt.show()
'''



'''
#reading dfs
features_path2 = path + "dengue_features_test.csv"
df_test = pd.read_csv(features_path2)
dfs_test,old_len_predictors = pre_process_data(df = df_test)

#predictions
#para cada modelo

predictions = []
for df_test,best_model,process,back_transform in zip(dfs_test,[best_iq_model,best_sj_model],[pre_process_iq,pre_process_sj],[back_transform_iq_y,back_transform_sj_y]):
    df_test = df_test.copy()
    to_predict = process(df_test,get_response=False)
    df_test['total_cases'] = back_transform(best_model.predict(to_predict))
    predictions.append(df_test['total_cases'])
    
    fig = plt.figure(figsize=(26, 10), dpi=100)
    ticks = np.arange(len(df_test))
    plt.plot(ticks,df_test['total_cases'],c='r',label = "Predicts")
    plt.legend()
    plt.show()

#reverse pred -> sj iq   
predictions = predictions[::-1]


#SAVING SUMISSION FILE
submission = pd.read_csv(path + "submission_format.csv",
                         index_col=[0, 1, 2])
result_path = "result/"
submission.total_cases = np.concatenate(predictions)
submission.to_csv(result_path+"result.csv")
'''