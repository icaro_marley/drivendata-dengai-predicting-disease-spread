# -*- coding: utf-8 -*-
"""
Created on Sun May  7 00:07:53 2017

@author: silvia-pc
"""

# -*- coding: utf-8 -*-
"""
Created on Sat May  6 23:09:47 2017

@author: icaromarley5
"""

'''
add modifications to this pipeline and test against the test data to see what are the ones valid and
what aren't
the test data may be very different from the training data, so I need to check and test everything that could be usefull.


things to add for now
adapt the code
split outliers
remove outlier detection
run linear model
select other variables
run random forests
run linear models
run linear models on sqrt y log x variables

'''

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats
import seaborn as sns
import winsound
from sklearn.externals import joblib
from sklearn.ensemble import RandomForestRegressor
from sklearn import preprocessing
from sklearn.preprocessing import PolynomialFeatures
from sklearn import linear_model
from sklearn.preprocessing import PolynomialFeatures,StandardScaler
from sklearn.model_selection import GridSearchCV

#preprocess data and returns 2 dataframes with Iquitos and San Juan Data
def pre_process_data(df,response=None,weeks_before=5): 
    #splitting to model the 2 cities
    df = df.copy()
    columns = ['week_start_date']
    df.drop(columns,axis=1,inplace=True)
    
    #'''remover
    if response != None:
        df = df[df[response] <50]
    #'''
    df_train_iq = df[df['city'] == 'iq'].copy()
    df_train_sj = df[df['city'] == 'sj'].copy()
    
    '''
    if response != None:
        #replace this values with mean
        #df_train_sj = df_train_sj[df_train_sj[response]<90]###linha adicionada
        mean_outlier =  df_train_sj[response][(df_train_sj[response]>50) & (df_train_sj[response]<90)].mean()
        df_train_sj.loc[df_train_sj[response]>90,response] = mean_outlier + df_train_sj[response][df_train_sj[response]>90].mod(30)
        #print(df_train_sj[response][df_train_sj[response]>60].mean())#71
    '''
    #getting values from past weeks
    dfs_train = [df_train_iq,df_train_sj]
    
    for df_train in dfs_train:
        df_train.drop(['city'],inplace=True,axis=1)
    
    
    columns = df_train_sj.columns.values
    if response != None:
        columns = np.delete(columns,df_train.columns.get_loc(response))
        
  
    old_len_columns = len(columns) 
    
    
    for n in range(1,weeks_before+1):
         for df_train in dfs_train:  
             for column in columns:
                 df_train[column+"_"+ str(n)] = df_train[column].shift(n)

    #dealing with na values -> fill with mean
    for df_train in dfs_train:
        df_train.fillna(df_train.mean(),inplace=True)

    if response != None:
        predictors = np.delete(df_train.columns,df_train.columns.get_loc(response))
    else:
        predictors = df_train.columns
    return dfs_train,predictors,old_len_columns



#reading training files
path = "../modified data/"
features_path = path + "dengue_features_train.csv"
labels_path = path + "dengue_labels_train.csv"

df_feat = pd.read_csv(features_path)
df_lab = pd.read_csv(labels_path)

#merge
df_merge = pd.merge(df_feat,df_lab,on=["city","year","weekofyear"])


#selecting response
response = "total_cases"

dfs_train,predictors,old_len_columns = pre_process_data(df_merge,response)


#training
def train(x,y,estimator,params):
    clf = GridSearchCV(estimator, params,scoring="neg_mean_absolute_error",cv=10)#,verbose=10000000000)
    clf.fit(x,y)
    return clf.best_estimator_,clf.best_score_  


#iq -6.32194232033
df_train = dfs_train[0].copy()

def back_transform_iq_y(values):
    return values.round(0).astype(int)

#return x,y
def pre_process_iq(df,get_response=True):
    explanatory = ['ndvi_ne', 'ndvi_nw',
       'reanalysis_specific_humidity_g_per_kg', 'reanalysis_tdtr_k'
       ]
    
    x = df[explanatory].copy()
    #scaler = preprocessing.StandardScaler().fit(x)
    #x = scaler.transform(x) 

    if get_response:
        y = df[response].copy()
        return x,y

    return x



x,y = pre_process_iq(df_train)

#random forest
estimator = RandomForestRegressor(
oob_score=False,
min_weight_fraction_leaf = 0.12700000000000003,
min_impurity_split = 1.0001e-06,
n_estimators=535,
random_state= 100,
)

params = {
}

best_iq_model,score = train(x,y,estimator,params)
print(score)


'''
#random forest
estimator = RandomForestRegressor(
#warm_start=True,
oob_score=False,
min_weight_fraction_leaf = 0.12700000000000003,
min_impurity_split = 1.0001e-06,
n_estimators=535,
random_state= 100,
)

#estimator = AdaBoostRegressor(estimator,random_state=100)
#estimator = BaggingRegressor(estimator,random_state=100)

parameters = {
#'min_impurity_split': [9.9999999999999995e-07]+ np.arange(1e-10,1e-7,1e-10)
}


clf = GridSearchCV(estimator, parameters,scoring="neg_mean_absolute_error",cv=10,verbose=10000000000)
clf.fit(x,y)
print("best score",clf.best_score_)
print("parameters",clf.best_params_)
'''

#sj 
df_train = dfs_train[1].copy()

def back_transform_sj_y(values):
    return values.round(0).astype(int)

#return x,y
def pre_process_sj(df,get_response=True):
    explanatory = ['ndvi_ne', 'ndvi_nw',
       'reanalysis_specific_humidity_g_per_kg', 'reanalysis_tdtr_k'
       ]
    
    x = df[explanatory].copy()
    #scaler = preprocessing.StandardScaler().fit(x)
    #x = scaler.transform(x) 

    if get_response:
        y = df[response].copy()
        return x,y

    return x



x,y = pre_process_sj(df_train)

#random forest
estimator = RandomForestRegressor(
oob_score=False,
min_weight_fraction_leaf = 0.12700000000000003,
min_impurity_split = 1.0001e-06,
n_estimators=535,
random_state= 100,
)

params = {
}

best_sj_model,score = train(x,y,estimator,params)
print(score)





'''
#reading dfs
features_path2 = path + "dengue_features_test.csv"
df_test = pd.read_csv(features_path2)
dfs_test,predictors,old_len_predictors = pre_process_data(df = df_test)

#predictions
#para cada modelo

predictions = []
for df_test,best_model,process,back_transform in zip(dfs_test,[best_iq_model,best_sj_model],[pre_process_iq,pre_process_sj],[back_transform_iq_y,back_transform_sj_y]):
    df_test = df_test.copy()
    to_predict = process(df_test,get_response=False)
    df_test['total_cases'] = back_transform(best_model.predict(to_predict))
    predictions.append(df_test['total_cases'])
    
    fig = plt.figure(figsize=(26, 10), dpi=100)
    ticks = np.arange(len(df_test))
    plt.plot(ticks,df_test['total_cases'],c='r',label = "Predicts")
    plt.legend()
    plt.show()
    

#SAVING SUMISSION FILE
submission = pd.read_csv(path + "submission_format.csv",
                         index_col=[0, 1, 2])
result_path = "result/"
submission.total_cases = np.concatenate(predictions)
submission.to_csv(result_path+"result.csv")
'''

