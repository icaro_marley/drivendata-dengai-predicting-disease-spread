# -*- coding: utf-8 -*-
"""
Created on Sat May  6 23:09:47 2017

@author: icaromarley5



rfr
explanatory = [#log
    'station_min_temp_c_7',
    'reanalysis_min_air_temp_k_8',
    ] -19.0685811133   -> custom scoring -> 19.040521978 TEST SCORE 25.7091

applied log x and sqrt y -> custom scoring 15.1501873556 

SVR -> -18.0404858557 -> TEST SCORE 27.2933
"""


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats
import seaborn as sns
import winsound
from sklearn.externals import joblib
from sklearn.ensemble import RandomForestRegressor
from sklearn import preprocessing
from sklearn.preprocessing import PolynomialFeatures
from sklearn import linear_model
from sklearn.preprocessing import PolynomialFeatures,StandardScaler
from sklearn.model_selection import GridSearchCV
from sklearn.svm import SVR

#preprocess data and returns 1 df
def pre_process_data(df,response=None,weeks_before=15): 
    #splitting to model the 2 cities
    df = df.copy()

    columns = ['week_start_date']
    df.drop(columns,axis=1,inplace=True)
    
    
    columns = df.columns.values
    if response != None:
        columns = np.delete(columns,df.columns.get_loc(response))
        columns = np.delete(columns,df.columns.get_loc('city'))
  
    old_len_columns = len(columns) 

    #dealing with na values 
    df.fillna(method='ffill',inplace=True) 
    
    for n in range(1,weeks_before+1):
        for column in columns:
            df[column+"_"+ str(n)] = df[column].shift(n)

    #dealing with na values for the shift operation
    df.fillna(method='bfill',inplace=True)#columns shifted 
    
    #one hot encoding for city
    df_city = pd.get_dummies(df['city']).astype(float)
    df.drop(['city'], axis = 1, inplace = True)
    df = df_city.join(df)
    
    if response != None:
        predictors = np.delete(df.columns,df.columns.get_loc(response))
    else:
        predictors = df.columns
    return df,predictors,old_len_columns



#reading training files
path = "../modified data/"
features_path = path + "dengue_features_train.csv"
labels_path = path + "dengue_labels_train.csv"

df_feat = pd.read_csv(features_path)
df_lab = pd.read_csv(labels_path)

#merge
df_merge = pd.merge(df_feat,df_lab,on=["city","year","weekofyear"])


#selecting response
response = "total_cases"

df_train,predictors,old_len_columns = pre_process_data(df_merge,response)



#training
def train(x,y,estimator,params):
    clf = GridSearchCV(estimator, params,scoring='neg_mean_absolute_error',cv=10)#,verbose=10000000000)
    clf.fit(x,y)
    print(clf.best_params_)
    return clf.best_estimator_,clf.best_score_  


def back_transform_y(values):
    return values.round(0).astype('int')

#return x,y
def pre_process(df,get_response=True):   
    explanatory = [
    #'reanalysis_dew_point_temp_k_8',
    'station_min_temp_c_7',
    'reanalysis_min_air_temp_k_8',
    #'reanalysis_air_temp_k_8',
    ]
    
    x = df[explanatory].copy()

    if get_response:
        y = df[response].copy()
        return x,y
    return x


x,y = pre_process(df_train)



#random forest
estimator = RandomForestRegressor(
oob_score=False,
min_weight_fraction_leaf = 0.12700000000000003,
min_impurity_split = 1.0001e-06,
n_estimators=535,
random_state= 100,
n_jobs = -1,
#criterion='mae'#-17.1229729383
)



from sklearn.ensemble import GradientBoostingRegressor
estimator = GradientBoostingRegressor()



params = {
#'n_estimators':np.append(np.arange(200,500),20)
}

best_model,score = train(x,y,estimator,params)
print(score)

from sklearn.metrics import mean_absolute_error
pred = best_model.predict(x)
true = df_train[response]
print("full dataset score", - mean_absolute_error(true,pred))

#plotando valores reais contra previsão
fig = plt.figure(figsize=(7, 5), dpi=100)
ticks = np.arange(len(df_train))
y_true = df_train['total_cases']
y_pred = back_transform_y(best_model.predict(x))
plt.plot(ticks,y_true,c='b',label = "Real Values")
plt.plot(ticks,y_pred,c='r',label = "Predicts")
plt.legend()
plt.show()






'''
#reading dfs
features_path2 = path + "dengue_features_test.csv"
df_test = pd.read_csv(features_path2)
df_test,predictors,old_len_predictors = pre_process_data(df = df_test)


#predictions
#para cada modelo

predictions = []
for best_model,process,back_transform in zip([best_model],[pre_process],[back_transform_y]):
    df_test = df_test.copy()
    to_predict = process(df_test,get_response=False)
    df_test['total_cases'] = back_transform(best_model.predict(to_predict))
    predictions.append(df_test['total_cases'])
    
    fig = plt.figure(figsize=(20, 10), dpi=100)
    ticks = np.arange(len(df_test))
    plt.plot(ticks,df_test['total_cases'],c='r',label = "Predicts")
    plt.legend()
    plt.show()
    

#SAVING SUMISSION FILE
submission = pd.read_csv(path + "submission_format.csv",
                         index_col=[0, 1, 2])
result_path = "result/"
submission.total_cases = np.concatenate(predictions)
submission.to_csv(result_path+"result.csv")
'''

