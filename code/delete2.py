print('\nSAN JUAN MODEL TRAINING\n')
df_train = dfs_train[1]
def back_transform_sj_y(values):
    return values.round(0).astype(int)

#return x
def pre_process_sj(df,get_response=True):
    explanatory = [
     'reanalysis_avg_temp_k_10',
     'station_avg_temp_c_10',
     'station_avg_temp_c_12',
     ]
    
    
    x = df[explanatory].copy()
    
    scaler = preprocessing.StandardScaler().fit(x)
    x = scaler.transform(x) 
    poly = PolynomialFeatures(degree=2)
    x = poly.fit_transform(x)
    
    if get_response:
        y = df[response].copy()
        return x,y
    return x

x_sj,y_sj = pre_process_sj(df_train)
x = x_sj
y = y_sj



estimator = NuSVR(nu=.83,C=10) 
   

params = {

}


# Choose cross-validation techniques for the inner and outer loops,
# independently of the dataset.
# E.g "LabelKFold", "LeaveOneOut", "LeaveOneLabelOut", etc.
inner_cv = KFold(n_splits=10, shuffle=True, random_state=100)
outer_cv = KFold(n_splits=4, shuffle=True, random_state=100)

# Non_nested parameter search and scoring
best_sj_model,best_params,score = train(x,y,estimator,params,inner_cv)
#print("inside CV score",score,best_params)

#full data score
pred = best_sj_model.predict(x)
true = df_train[response]
full_score = - mean_absolute_error(true,pred)
#print("full dataset score",full_score)

#print('difference',abs(full_score - score))
print("cv error",score)
print("full data error",full_score)