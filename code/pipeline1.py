# -*- coding: utf-8 -*-
"""
Created on Wed Apr 26 17:29:21 2017

@author: silvia-pc
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import GridSearchCV

path = "../modified data/"
features_path = path + "dengue_features_train.csv"
labels_path = path + "dengue_labels_train.csv"

df_feat = pd.read_csv(features_path)
df_lab = pd.read_csv(labels_path)


#merge
df_train = pd.merge(df_feat,df_lab,on=["city","year","weekofyear"])
#index on ["city","year","weekofyear"]
df_train.set_index(["city","year","weekofyear"],inplace=True)

#selecting variables
'''
station_max_temp_c – Maximum temperature
station_min_temp_c – Minimum temperature
station_avg_temp_c – Average temperature
station_precip_mm – Total precipitation

reanalysis_relative_humidity_percent – Mean relative humidity
reanalysis_specific_humidity_g_per_kg Mean specific humidity
'''



response = "total_cases"
'''
predictors = df_train.columns

predictors = np.delete(predictors,-1)
predictors = np.delete(predictors,0)

'''
predictors = ['ndvi_ne', 'ndvi_nw',
       'reanalysis_specific_humidity_g_per_kg', 'reanalysis_tdtr_k'
       ]

df_train = df_train[list(predictors)+[response]]

#dropping na values
df_train.dropna(inplace=True)
#new shape
#df_train.shape

#df_train.describe()



#df_train[response].value_counts()


#hist plot
plt.hist(df_train[response], 50)
plt.title("Histogram of total dengue fever cases")
plt.show()

#outlier removal
df_train = df_train[df_train[response]<90]

'''
#hex bin plot
len_side = 3
f, axarr = plt.subplots(int(len(predictors)/len_side),len_side,figsize=(16,10),sharey=True)
f.subplots_adjust(hspace=.3)
df_train.index
side = 0
index = 0


predictors_names = ["Maximum temperature",
"Minimum temperature","Average temperature",
"Total precipitation","Mean relative humidity",
"Mean specific humidity"]

for predictor,name in zip(predictors,predictors_names):
    x = df_train[predictor]
    y = df_train[response]
    #axarr[index,side].hexbin(x,y,gridsize=40, cmap='YlOrRd')
    axarr[index,side].scatter(x,y,c='b')
    #possible data transformation?
    #axarr[index,side].scatter(x.apply(lambda x:np.log(x)),y,c='r')
    axarr[index,side].set_xlabel(name)
    axarr[index,side].set_ylabel("Total dengue fever cases")
    corr = scipy.stats.pearsonr(x, y)
    axarr[index,side].set_title("Pearson Test R {:.2f} P {:.2f}".format(corr[0],corr[1]))
    side+=1
    if side > len_side - 1:
        side = 0
        index+=1

plt.suptitle("Total dengue fever cases by predictos")
#f.savefig('to.png') 
plt.show()
'''
print("training")
x = df_train[predictors]
y = df_train[response]


from sklearn.ensemble import AdaBoostRegressor
from sklearn.ensemble import BaggingRegressor

'''
#random forest
estimator = RandomForestRegressor(
#warm_start=True,
oob_score=False,
min_weight_fraction_leaf = 0.12700000000000003,
min_impurity_split = 1.0001e-06,
n_estimators=535,
random_state= 100,
)

#estimator = AdaBoostRegressor(estimator,random_state=100)
#estimator = BaggingRegressor(estimator,random_state=100)

parameters = {
#'min_impurity_split': [9.9999999999999995e-07]+ np.arange(1e-10,1e-7,1e-10)
}


clf = GridSearchCV(estimator, parameters,scoring="neg_mean_absolute_error",cv=10,verbose=10000000000)
clf.fit(x,y)
print("best score",clf.best_score_)
print("parameters",clf.best_params_)
'''

'''
#improving random state model
best_score = -11.3623669981
while (True):
    clf = GridSearchCV(estimator, parameters,scoring="neg_mean_squared_error",cv=10)#verbose=10000000000
    clf.fit(x,y)
    if clf.best_score_ > best_score:
        print("better model found")
        best_score = clf.best_score_
        print("best score",clf.best_score_)
        best_model = clf.best_estimator_
        print("parameters",clf.best_estimator_)
        print()
'''


#clf.cv_results_['mean_test_score']


'''
import winsound
winsound.Beep(300,1000)
'''
#best score -259.882343194
'''

#feature importances
importances = clf.best_estimator_.feature_importances_
features = np.array(predictors)
indices = np.argsort(importances)
 
plt.figure(1)
plt.title('Feature Importances')
plt.barh(range(len(indices)), importances[indices], color='b', align='center')
plt.yticks(range(len(indices)), 
           features[indices])
plt.xlabel('Relative Importance')
plt.show()

'''
'''
#trees visualization
import subprocess
from sklearn import tree
i_tree = 0
for tree_in_forest in clf.best_estimator_.estimators_:
    name = 'tree_' + str(i_tree)
    with open(name + '.dot', 'w') as my_file:
        my_file = tree.export_graphviz(tree_in_forest, out_file = my_file)
    i_tree = i_tree + 1
    subprocess.call("dot -Tpng  {0}.dot -o {0}.png".format(name),shell=True)
    #os.system()
'''

'''
#feature selection
from sklearn.feature_selection import SelectFromModel


model = SelectFromModel(clf.best_estimator_, prefit=True)
x_new = model.transform(x)
x_new.shape               

x = x_new

'''

'''
#RESULT

estimator = RandomForestRegressor(
#warm_start=True,
oob_score=False,
min_weight_fraction_leaf = 0.12700000000000003,
min_impurity_split = 1.0001e-06,
n_estimators=535,
random_state= 100,
)


estimator.fit(x,y)
features_path2 = path + "dengue_features_test.csv"
df_test = pd.read_csv(features_path2)
#index on ["city","year","weekofyear"]
df_test.set_index(["city","year","weekofyear"],inplace=True)

df_test.describe()

df_test = df_test[predictors].fillna(df_test.mean())#.astype('float32')

df_test['total_cases'] = estimator.predict(df_test)

df_result = df_test['total_cases'].reset_index()

df_result['total_cases'] = df_result['total_cases'].astype(int)


result_path = "result/"
df_result.to_csv(result_path+"result.csv",index=False)

#saving
#from sklearn.externals import joblib
#model_path = "models/"
#joblib.dump(estimator, model_path+ 'FIRSTSUB_RFR.pkl') 
#clf = joblib.load('filename.pkl')



submission_path = path + "submission_format.csv"
file = open(submission_path, "r") 
print (file.read()) 
file.close()


submissiondf = pd.read_csv(submission_path)
'''
