# -*- coding: utf-8 -*-
"""
Created on Sat May  6 23:09:47 2017

@author: icaromarley5
"""


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats
import seaborn as sns
import winsound
from sklearn.externals import joblib
from sklearn.ensemble import RandomForestRegressor, ExtraTreesRegressor
from sklearn import preprocessing
from sklearn.preprocessing import PolynomialFeatures
from sklearn import linear_model
from sklearn.preprocessing import PolynomialFeatures,StandardScaler
from sklearn.model_selection import GridSearchCV
#np.set_printoptions(threshold='nan')
#pd.set_option('display.max_rows', None)
#preprocess data and returns 1 df
def pre_process_data(df,response=None,weeks_before=12): 
    #splitting to model the 2 cities
    df = df.copy()

    columns = ['week_start_date']
    df['week_start_date'] = pd.to_datetime(df['week_start_date'])
    df.set_index('week_start_date',inplace=True)
    #df.drop(columns,axis=1,inplace=True)
    

    
    columns = df.columns.values
    if response != None:
        columns = np.delete(columns,df.columns.get_loc(response))
        columns = np.delete(columns,df.columns.get_loc('city'))
  
    old_len_columns = len(columns) 

    #dealing with na values 
    df.fillna(method='ffill',inplace=True) 
    
    for n in range(1,weeks_before+1):
        for column in columns:
            df[column+"_"+ str(n)] = df[column].shift(n)

    #dealing with na values for the shift operation
    df.fillna(method='bfill',inplace=True)#columns shifted 
    
        
    df_train_iq = df[df['city'] == 'iq'].copy()
    df_train_sj = df[df['city'] == 'sj'].copy()    
    df_train_iq.drop('city',axis=1,inplace=True)
    df_train_sj.drop('city',axis=1,inplace=True)    
        
    return [df_train_iq,df_train_sj],old_len_columns



#reading training files
path = "../modified data/"
features_path = path + "dengue_features_train.csv"
labels_path = path + "dengue_labels_train.csv"

df_feat = pd.read_csv(features_path)
df_lab = pd.read_csv(labels_path)

#merge
df_merge = pd.merge(df_feat,df_lab,on=["city","year","weekofyear"])


#selecting response
response = "total_cases"

dfs_train,old_len_columns = pre_process_data(df_merge,response)


'''
#hist plot
f, axarr = plt.subplots(1,2,sharex=True,sharey=True,figsize=(7,4))
index = 0 
for city,df_train in zip(["Iquitos","San Juan"],dfs_train):
    f.subplots_adjust(hspace=.3)
    axarr[index].hist(df_train[response], 50)
    axarr[index].set_title(city)
    index+=1
plt.suptitle("Histogram of total dengue fever cases")
plt.show()


#line plot
f, axarr = plt.subplots(1,2,sharey=True,sharex=True,figsize=(7,4))
index = 0 
for city,df_train in zip(["Iquitos","San Juan"],dfs_train):
    #df_train.set_index(['weekofyear','year'],inplace=True)
    f.subplots_adjust(hspace=.3)
    axarr[index].plot(df_train[response])
    axarr[index].set_title(city)
    index+=1
    #df_train.reset_index()
plt.suptitle("Line Plot of total dengue fever cases")
plt.show()

#correlations
for city,df_train in zip(["Iquitos","San Juan"],dfs_train):
    corrmat = df_train.corr()
    fig = plt.figure(figsize=(50, 10), dpi=100)
    ticks = np.arange(len(corrmat['total_cases']))
    plt.plot(ticks,corrmat['total_cases'])
    labels = corrmat['total_cases'].index.values
    plt.xticks(ticks, labels, rotation='vertical')
    plt.title(city)
    plt.show()

for df_train in dfs_train:
    corr = df_train.corr(method='spearman')['total_cases']
    corr.drop('total_cases',inplace=True)
    print(corr.sort_values(ascending = False)[:20])


     




pearson analysis (linear correlation)
iq model
weekofyear_10                              0.280907 bad
reanalysis_specific_humidity_g_per_kg      0.234918 bad
reanalysis_dew_point_temp_k                0.228864 bad

sj model
station_avg_temp_c_10    0.372402 meh
station_min_temp_c_10    0.365743 bad
station_max_temp_c_11    0.320711 meh
reanalysis_min_air_temp_k_8                 0.306086 ok
reanalysis_dew_point_temp_k_8               0.305654 meh
reanalysis_specific_humidity_g_per_kg_8     0.305335 meh
reanalysis_air_temp_k_8                     0.301337 meh


spearman (monotonic read more)
iq
reanalysis_specific_humidity_g_per_kg      0.369693 bad
reanalysis_dew_point_temp_k                0.365853 bad
year                                       0.329163 bad
reanalysis_min_air_temp_k                  0.311049 bad

sj
station_avg_temp_c_12                       0.575803 meh
station_min_temp_c_10                       0.537087 bad
reanalysis_specific_humidity_g_per_kg_9     0.529533 meh
reanalysis_dew_point_temp_k_9               0.529306 meh
station_max_temp_c_12                       0.516217 meh


kendall
iq

sj
station_avg_temp_c_12                       0.408434 meh
station_min_temp_c_10                       0.392415 bad
station_max_temp_c_12                       0.374053 meh
reanalysis_specific_humidity_g_per_kg_8     0.363999 meh
reanalysis_dew_point_temp_k_9               0.363942 meh





#usar para hexbin tbm
#scatter plot predictor x reponse by medition week 
len_side = 6
predictors = np.delete(df_train.columns,df_train.columns.get_loc(response))
predictors_len = len(predictors)
for city,df_train in zip(["Iquitos","San Juan"],dfs_train):
    df_train.dropna(inplace=True)
    step = old_len_columns
    limit_inf = 0
    limit_max = step
    week = 0
    while (limit_max<=predictors_len):
        index = 0
        side = 0
        selected = predictors[limit_inf:limit_max]
        
        f, axarr = plt.subplots(int(len(selected)/len_side) + 1,len_side,
                                figsize=(19,14),sharey=True)
        f.subplots_adjust(hspace=.5)
        for predictor in selected:
            x = df_train[predictor]#.apply(lambda x:np.sqrt(x))
            y = df_train[response]#.apply(lambda x: np.power(x,1/4))
            #print(predictor,index,side)
            axarr[index,side].scatter(x,y)#,cmap='inferno')
            axarr[index,side].set_xlabel(predictor)
            corr = scipy.stats.pearsonr(x,y)
            axarr[index,side].set_title("Pearson R {:.2f} P {:.2f}".format(corr[0],corr[1]))
            side+=1
            if side > len_side - 1:
                side = 0
                index+=1   
        plt.suptitle(city + ": total dengue fever cases by predictors week " + str(week))
        plt.show()  
        
        #print(limit_inf,limit_max,selected,step)
        limit_inf = limit_max
        limit_max += step
        week+= -1
        
    
#montar funcoes de treino e back para cada modelo
testar variaveis
moficicar funcoes de gravacao no final
'''

#TRAINING
def train(x,y,estimator,params):
    clf = GridSearchCV(estimator, params,scoring="neg_mean_absolute_error",cv=10,verbose=10000000000)
    clf.fit(x,y)
    #print(clf.best_params_)
    #winsound.Beep(300,300)
    return clf.best_estimator_,clf.best_params_,clf.best_score_  


'''
IQUITOS 

'''

print('\nIQUITOS MODEL TRAINING\n')
df_train = dfs_train[0]
def back_transform_iq_y(values):
    return values.round(0).astype(int)




#return x
def pre_process_iq(df,get_response=True):
    explanatory = [
    'reanalysis_specific_humidity_g_per_kg',
    #'reanalysis_dew_point_temp_k',
    #'reanalysis_min_air_temp_k',
    #'weekofyear_10',
    #'weekofyear_12',
    #'weekofyear_8',
    
    ]
    
    x = df[explanatory].copy()
    scaler = preprocessing.StandardScaler().fit(x)
    x = scaler.transform(x) 
    #poly = PolynomialFeatures(degree=)
    #x = poly.fit_transform(x)
    
    if get_response:
        y = df[response].copy()
        return x,y

    return x


x_iq,y_iq = pre_process_iq(df_train)
x = x_iq
y = y_iq
estimator = RandomForestRegressor(random_state=100,n_estimators=5,n_jobs = -1,
)
#-6.84406407977 on weekofyear_10 not well fitted


'''
estimator = linear_model.LinearRegression()
#-6.5622287214 on reanalysis_specific_humidity_g_per_kg   underfit maybe

estimator = GradientBoostingRegressor(random_state=100, n_estimators=12)
#-6.52197229112  ok fit
'''

params = {

}



best_iq_model,best_params,score = train(x,y,estimator,params)
print(score,best_params)







'''
SAN JUAN  -25.4676282051
'''
print('\nSAN JUAN MODEL TRAINING\n')
df_train = dfs_train[1]
def back_transform_sj_y(values):
    return values.round(0).astype(int)

#return x
def pre_process_sj(df,get_response=True):
    explanatory = [
    'station_max_temp_c_12',
    'reanalysis_dew_point_temp_k_9',
    'reanalysis_specific_humidity_g_per_kg_9',
    'reanalysis_dew_point_temp_k_8',
    'station_min_temp_c_10',
    'station_max_temp_c_11',
    'reanalysis_specific_humidity_g_per_kg',
    'year',
    'reanalysis_dew_point_temp_k',
    'reanalysis_min_air_temp_k',
    ]
    
    x = df[explanatory].copy()
    #scaler = preprocessing.StandardScaler().fit(x)
    #x = scaler.transform(x) 

    if get_response:
        y = df[response].copy()
        return x,y

    return x


x_sj,y_sj = pre_process_sj(df_train)
x = x_sj
y = y_sj


'''
estimator = RandomForestRegressor(random_state=100,#
#criterion='mae'
)
params = {
#'n_estimators': np.append(np.arange(50,100),24)     
        }
'''


from sklearn.ensemble import GradientBoostingRegressor
estimator = GradientBoostingRegressor(random_state=100,n_estimators=20)#-25.0726242515


params = {
#'n_estimators':np.append(np.arange(200,600),20)
}



best_sj_model,best_params,score = train(x,y,estimator,params)
print(score,best_params)

'''

df_train = dfs_train[0]

#feature selection (backwards elimination) rfr
best_score = -17.6616619913
x,y = pre_process(df_train)
explanatory = list(x.columns)
old_len = 0
new_len = len(explanatory)
while (old_len != new_len):
    print("TENTANDO NOVAMENTE")
    i = 0
    while (i<len(explanatory)):
        old_len = new_len
        print(i)
        selected = explanatory.pop(i)
        x_local = x[explanatory]

        estimator = estimator = RandomForestRegressor(
        random_state= 100,
        #criterion='mae'
        )
        params = {}
        best__model,score = train(x,y,estimator,params)
        if score >= best_score:
            best_score = score
            print("best score so far",best_score,"len",len(explanatory))
        else:
            explanatory.insert(i,selected)
            i+=1
    
    new_len = len(explanatory)
  

print(best_score)
print(explanatory)
winsound.Beep(300,300)

'''






















#plot line plot from predictions 
#and real values

#plotando valores reais contra previsão
fig = plt.figure(figsize=(7, 5), dpi=100)
y_true = dfs_train[1]['total_cases'].copy().append(dfs_train[0]['total_cases'])
sj_pred = back_transform_sj_y(best_sj_model.predict(x_sj))
iq_pred = back_transform_iq_y(best_iq_model.predict(x_iq))
ticks = np.arange(len(y_true))
y_pred = np.concatenate([sj_pred,iq_pred])
plt.plot(ticks,y_true,c='b',label = "Real Values")
plt.plot(ticks,y_pred,c='r',label = "Predicts")
plt.legend()
plt.show()




'''
#reading dfs
features_path2 = path + "dengue_features_test.csv"
df_test = pd.read_csv(features_path2)
dfs_test,old_len_predictors = pre_process_data(df = df_test)

#predictions
#para cada modelo

predictions = []
for df_test,best_model,process,back_transform in zip(dfs_test,[best_iq_model,best_sj_model],[pre_process_iq,pre_process_sj],[back_transform_iq_y,back_transform_sj_y]):
    df_test = df_test.copy()
    to_predict = process(df_test,get_response=False)
    df_test['total_cases'] = back_transform(best_model.predict(to_predict))
    predictions.append(df_test['total_cases'])
    
    fig = plt.figure(figsize=(26, 10), dpi=100)
    ticks = np.arange(len(df_test))
    plt.plot(ticks,df_test['total_cases'],c='r',label = "Predicts")
    plt.legend()
    plt.show()
    
predictions = predictions[::-1]

####inverter pred aqui -> sj iq
#SAVING SUMISSION FILE
submission = pd.read_csv(path + "submission_format.csv",
                         index_col=[0, 1, 2])
result_path = "result/"
submission.total_cases = np.concatenate(predictions)
submission.to_csv(result_path+"result.csv")
'''