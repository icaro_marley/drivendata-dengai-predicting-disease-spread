# -*- coding: utf-8 -*-
"""
Created on Fri Aug  3 14:58:09 2018

@author: icaromarley5
"""

'''
http://philipperemy.github.io/keras-stateful-lstm/
https://fairyonice.github.io/Stateful-LSTM-model-training-in-Keras.html
http://colah.github.io/posts/2015-08-Understanding-LSTMs/
https://machinelearningmastery.com/stateful-stateless-lstm-time-series-forecasting-python/
https://github.com/keras-team/keras/issues/7212
https://fairyonice.github.io/Understand-Keras's-RNN-behind-the-scenes-with-a-sin-wave-example.html
https://fairyonice.github.io/Extract-weights-from-Keras's-LSTM-and-calcualte-hidden-and-cell-states.html

ler stateful
    decidir se implementa.
ver pydata
'''

# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
from keras.models import Sequential, model_from_json
from keras.layers import Dense, LSTM, Dropout, Activation,Lambda,BatchNormalization
from keras.callbacks import Callback 
from sklearn.preprocessing import Imputer,StandardScaler
from keras.callbacks import ModelCheckpoint,EarlyStopping
import matplotlib.pyplot as plt
import seaborn as sns
from keras import regularizers
from keras.optimizers import rmsprop
from sklearn.decomposition import PCA
from sklearn.metrics import mean_absolute_error

data_path = "../modified data/"
results_path = '../results/'
random_state= 100

df_test_all = pd.read_csv(data_path+'dengue_features_test.csv',parse_dates=[3])
df_train_x = pd.read_csv(data_path + "dengue_features_train.csv",parse_dates=[3])
df_train_y = pd.read_csv(data_path + "dengue_labels_train.csv")
df_train_all = df_train_x.merge(df_train_y)
del df_train_x,df_train_y


target = "total_cases"

'''
dates already sorted
split between cities
'''
df_train_all.set_index('week_start_date',inplace=True)
df_test_all.set_index('week_start_date',inplace=True)
# nulls
df_train_all.isnull().any()

def get_previous_values(data,window_size):
    new_dataframe = pd.DataFrame()
    for i in range(0,window_size):
        df_aux = pd.DataFrame([])
        for i2 in range(data.shape[1]):
            df_aux[str(i2) + '_' + str(i)] = pd.Series(data[:,i2]).shift(i)
        new_dataframe = pd.concat([new_dataframe,df_aux],axis=1)
    return new_dataframe.values

# sj 
city = 'sj'
df_train = df_train_all[df_train_all['city']==city].copy()
df_test = df_test_all[df_test_all['city']==city].copy()

'''
# analysis
df_check = df_train[df_train['city']==city]
df_check = df_check.iloc[:df_check.shape[0]-48]

plt.plot(StandardScaler().fit_transform((df_check[[
#'reanalysis_specific_humidity_g_per_kg',
 'reanalysis_avg_temp_k',
 'station_avg_temp_c',
# target
 ]].fillna(0))))
    

df_check[[
'reanalysis_specific_humidity_g_per_kg',
 'reanalysis_avg_temp_k',
 'station_avg_temp_c'
]].corr()


sns.heatmap(df_train[columns+[target]].corr())


'weekofyear',final do ano geralmente ocorrem picos de casos dengue
'station_avg_temp_c', sem associação
'ndvi_nw','ndvi_ne','ndvi_sw',' 'ndvi_se', sem associação
precipitation_amt_mm checar melhor
reanalysis_air_temp_k reanalysis_max_air_temp_k reanalysis_min_air_temp_k reanalysis_avg_temp_k reanalysis_dew_point_temp_k sobe e a variavel aumenta depois ->  'reanalysis_avg_temp_k', faz mais sentido
reanalysis_precip_amt_kg_per_m2  reanalysis_sat_precip_amt_mm  tem alguma associação lagada, reanalysis_precip_amt_kg_per_m2 possui um padrão mais obvio
reanalysis_relative_humidity_percent reanalysis_specific_humidity_g_per_kg tem associação, escolhendo reanalysis_specific_humidity_g_per_kg por possuir um padrão mais definido
reanalysis_tdtr_k sem
'station_max_temp_c', station_min_temp_c  station_avg_temp_c com, lagada -> station_avg_temp_c faz mais sentido
 station_diur_temp_rng_c sem
station_precip_mm nao
total_cases_prev muito


correlations
reanalysis_specific_humidity_g_per_kg reanalysis_avg_temp_k station_avg_temp_c
considerando apenas station_avg_temp_c pois possui um padrão mais definido

aumentar janela para 1 ano ou 6 meses
'''
# preprocessing, 
# get previous target values
df_train[target+'_prev'] = df_train[target].shift(1)
df_test[target+'_prev'] = df_train[target][-1] # need to fill other values

# feature selection
columns = [
   'weekofyear', 
   'station_avg_temp_c',
   'reanalysis_precip_amt_kg_per_m2',
] + [target+'_prev'] 

X_train = df_train[columns].values
y_train = df_train[target].values
X_test = df_test[columns].values

df_predictions = df_test[['city','year','weekofyear']].copy()

to_predict = 48 
X_train = X_train[:X_train.shape[0] - to_predict]
y_train = y_train[:y_train.shape[0] - to_predict].reshape(-1,1)
X_val = X_train[X_train.shape[0] - to_predict:]
y_val = y_train[y_train.shape[0] - to_predict:].reshape(-1,1)

# fill
imp = Imputer(strategy='mean',axis=0).fit(X_train)
X_train = imp.transform(X_train)
X_val = imp.transform(X_val)
X_test = imp.transform(X_test)
# scaler
scaler_x = StandardScaler().fit(X_train)
X_train = scaler_x.transform(X_train)
X_val = scaler_x.transform(X_val)
X_test = scaler_x.transform(X_test)
scaler_y = StandardScaler().fit(y_train)
y_train = scaler_y.transform(y_train)
y_val = scaler_y.transform(y_val)

# shift
window = 4 * 1 #3  # 1 good
train_size = X_train.shape[0] + X_val.shape[0]
X = get_previous_values(np.append(np.append(X_train,X_val,axis=0),X_test,axis=0),window)
X_train = X[:X_train.shape[0]]
X_val = X[X_train.shape[0]:X_train.shape[0] + to_predict]
X_test = X[train_size:]
del X
# drop nulls from shift operation
X_train = X_train[window:]
y_train = y_train[window:]
np.isnan(X_train).any()

# reshape
X_train = X_train.reshape(X_train.shape[0],window,len(columns))
X_val = X_val.reshape(X_val.shape[0],window,len(columns))
X_test = X_test.reshape(X_test.shape[0],window,len(columns))

# batch training
model_name = city+'model_2'
n_runs = 5
df_scores_train = pd.DataFrame()
df_scores_valid = pd.DataFrame()

n_epochs = 1000
batch_size = X_train.shape[0]
best_score = np.Inf
n_runs = 10
for run in range(1,n_runs+1):
    model = Sequential()
    #model.add(LSTM(5,input_shape=(window, len(columns)),activation='relu',return_sequences=True))
    #model.add(LSTM(5,input_shape=(window, len(columns)),activation='relu',return_sequences=True))
    model.add(LSTM(5,input_shape=(window, len(columns)),activation='relu'))
    model.add(Dense(5,activation='relu'))
    #model.add(Dense(5,activation='relu'))
    model.add(Dense(1))
    model.compile(loss='mean_absolute_error', optimizer='rmsprop')
    train_loss_list = []
    val_loss_list = []
    for epoch in range(1,n_epochs+1):
        train_loss_aux = []
        for batch_max_index in range(batch_size,X_train.shape[0]+batch_size,batch_size):
            X_train_batch = X_train[batch_max_index-batch_size:batch_max_index]
            y_train_batch = y_train[batch_max_index-batch_size:batch_max_index]
            train_loss_aux.append(model.train_on_batch(X_train_batch,y_train_batch))
        train_loss = np.mean(train_loss_aux)
        predictions = []
        prediction = None
        for index_val in range(X_val.shape[0]):
            X_val_aux = X_val[index_val].copy()
            if prediction: # add prediction as features
                for i in range(0,len(predictions)):   
                    if i < X_val_aux.shape[0]:
                        X_val_aux[i,3] = predictions[i] 
                    else:
                        break
            prediction = model.predict(X_val[index_val].reshape(-1,X_val.shape[1],X_val.shape[2]))[0][0]
            predictions.append(prediction)
        val_loss = mean_absolute_error(y_val,predictions)
        train_loss_list.append(train_loss)
        val_loss_list.append(val_loss)
        print('Run {} Epoch {} loss {} val loss {}'.format(run, epoch, train_loss,val_loss))
        if train_loss < best_score:
            print('Best score found {},  saving weights at '.format(best_score) + results_path+model_name+'.hdf5')
            model.save_weights(results_path+model_name+'.hdf5')
            best_score = train_loss
        else:
            print('Score not improved from {}'.format(best_score))
    df_scores_train['loss_'+str(run)] = train_loss_list
    df_scores_valid['val_loss_'+str(run)] = val_loss_list

plt.plot(df_scores_train.index, df_scores_train.mean(axis=1),c='darkblue',label='Train Mean')
plt.fill_between(df_scores_train.index, df_scores_train.mean(axis=1)-df_scores_train.std(axis=1), df_scores_train.mean(axis=1)+df_scores_train.std(axis=1),label='Train Std')
plt.plot(df_scores_valid.index, df_scores_valid.mean(axis=1),c='darkred',label='Valid Mean')
plt.fill_between(df_scores_valid.index, df_scores_valid.mean(axis=1)-df_scores_valid.std(axis=1), df_scores_valid.mean(axis=1)+df_scores_valid.std(axis=1),label='Valid Std',color='r')
plt.legend()

'''
com train e valid:
> analisar plot e selecionar melhor modelo/ tradeoff
    . 
> executar n (30) vezes sob um callback e calcular train e valid medio
    . vai avaliar o desempenho do modelo sob o numero de epocas
    . plotar para ver 
'''

model.load_weights(results_path+model_name+'.hdf5')
model.compile(loss='mean_absolute_error', optimizer='rmsprop')
predictions = np.round((model.predict(X_test)))

'''
3.46092
model = Sequential()
model.add(LSTM(100,input_shape=(window, len(columns)),activation='relu',return_sequences=True))
model.add(LSTM(100,activation='relu'))
model.add(Dense(100,activation='relu'))
model.add(Dense(100,activation='relu'))
model.add(Dense(1,kernel_regularizer=regularizers.l1(0.1),bias_regularizer=regularizers.l1(0.1)))
model.compile(loss='mean_absolute_error', optimizer='rmsprop')
'''

plt.plot_date(df_train[df_train['city']==city].index,df_train[df_train['city']==city][target],'-',c='b',label='Data')
plt.plot_date(df_test[df_test['city']==city].index,model.predict(X_test),'-',c='r',label='Predictions')
plt.legend()
plt.title("City: " + city)










'''
# fit
model_name = 'sj_model2'
checkpoint = ModelCheckpoint(results_path+model_name+'.hdf5', monitor='val_loss', 
    verbose=1, save_best_only=True, save_weights_only=False,
    mode='auto', period=1)
earlystopping = EarlyStopping(monitor='val_loss', min_delta=0, patience=100,
    verbose=1, mode='auto')

.01835 sj_model
model = Sequential()
model.add(LSTM(100,input_shape=(window, len(columns)),activation='relu'))
model.add(Dense(100,activation='relu'))
model.add(Dense(1))
model.compile(loss='mean_absolute_error', optimizer='rmsprop')

history = model.fit(X_train, y_train, epochs=10000, batch_size=30,
    verbose=2,shuffle=False,validation_data=(X_val,y_val),callbacks=[checkpoint,earlystopping])
model.evaluate(X_val,y_val)
























# iq
city = 'iq'
columns = [column for column in df_train.columns if column not in  [target,'city']]
X_train = df_train[df_train['city']==city][columns].values
y_train = df_train[df_train['city']==city][target].values
X_test = df_test[df_test['city']==city][columns].values

df_predictions = df_test[['city','year','weekofyear']].copy()

X_train = X_train[:X_train.shape[0]]
y_train = y_train[:y_train.shape[0]].reshape(-1,1)

# fill
imp = Imputer(strategy='mean',axis=0).fit(X_train)
X_train = imp.transform(X_train)
X_test = imp.transform(X_test)
# scaler
scaler_x = StandardScaler().fit(X_train)
X_train = scaler_x.transform(X_train)
X_test = scaler_x.transform(X_test)

# shift
window = 12
train_size = X_train.shape[0]
X = get_previous_values(np.append(X_train,X_test,axis=0),window)
X_train = X[:X_train.shape[0]]
X_test = X[train_size:]

# drop nulls from shift operation
X_train = X_train[window:]
y_train = y_train[window:]

# reshape
X_train = X_train.reshape(X_train.shape[0],window,len(columns))
X_test = X_test.reshape(X_test.shape[0],window,len(columns))

# LSTM model
model_name = 'iq_model2'
checkpoint = ModelCheckpoint(results_path+model_name+'.hdf5', monitor='loss', 
    verbose=1, save_best_only=True, save_weights_only=False,
    mode='auto', period=1)
earlystopping = EarlyStopping(monitor='loss', min_delta=0, patience=100,
    verbose=1, mode='auto')

model = Sequential()
model.add(LSTM(100,input_shape=(window, len(columns)),activation='relu'))
model.add(Dense(100,activation='relu'))
model.add(Dense(1))
model.compile(loss='mean_absolute_error', optimizer='rmsprop')
history = model.fit(X_train, y_train, epochs=10000, batch_size=30,
    verbose=2,shuffle=False,callbacks=[checkpoint,earlystopping])

predictions = np.append(predictions,np.round((model.predict(X_test))),axis=0)
'''
iq_model2 0.03154
model.add(LSTM(100,input_shape=(window, len(columns)),activation='relu'))
model.add(Dense(100,activation='relu'))
model.add(Dense(1))
'''


df_predictions[target] = predictions.astype(int)
df_predictions.to_csv(results_path+"result.csv",index=False)


df_predictions.index = df_test['week_start_date']

city = 'iq'
data = df_train[df_train['city']==city]
pred = df_predictions[df_predictions['city']==city]
plt.plot_date(data.index,data[target],'-',c='slateblue',label='Data')
plt.plot_date(pred.index,pred[target],'-',c='tomato',label='Predictions')
plt.legend()

city = 'sj'
data = df_train[df_train['city']==city]
pred = df_predictions[df_predictions['city']==city]
plt.plot_date(data.index,data[target],'-',c='slateblue',label='Data')
plt.plot_date(pred.index,pred[target],'-',c='tomato',label='Predictions')
plt.legend()

"""
# -*- coding: utf-8 -*-
"""
Created on Fri Aug  3 14:58:09 2018

@author: icaromarley5
"""

# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
from keras.models import Sequential, model_from_json
from keras.layers import Dense, LSTM, Dropout, Activation,Lambda
from keras.callbacks import Callback 
from sklearn.preprocessing import Imputer,StandardScaler
from keras.callbacks import ModelCheckpoint,EarlyStopping
import matplotlib.pyplot as plt


data_path = "../modified data/"
results_path = '../results/'
random_state= 100

df_test = pd.read_csv(data_path+'dengue_features_test.csv',parse_dates=[3])
df_train_x = pd.read_csv(data_path + "dengue_features_train.csv",parse_dates=[3])
df_train_y = pd.read_csv(data_path + "dengue_labels_train.csv")
df_train = df_train_x.merge(df_train_y)
del df_train_x,df_train_y

target = "total_cases"

'''
dates already sorted
split between cities
'''
df_train.set_index('week_start_date',inplace=True)
# nulls
df_train.isnull().any()

def get_previous_values(data,window_size):
    new_dataframe = pd.DataFrame()
    for i in range(0,window_size):
        df_aux = pd.DataFrame([])
        for i2 in range(data.shape[1]):
            df_aux[str(i2) + '_' + str(i)] = pd.Series(data[:,i2]).shift(i)
        new_dataframe = pd.concat([new_dataframe,df_aux],axis=1)
    return new_dataframe.values

# sj 
city = 'sj'
columns = [column for column in df_train.columns if column not in  [target,'city']]
X_train = df_train[df_train['city']==city][columns].values
y_train = df_train[df_train['city']==city][target].values
X_test = df_test[df_test['city']==city][columns].values

df_predictions = df_test[['city','year','weekofyear']].copy()

to_predict = 48
X_train = X_train[:X_train.shape[0] - to_predict]
y_train = y_train[:y_train.shape[0] - to_predict].reshape(-1,1)
X_val = X_train[X_train.shape[0] - to_predict:]
y_val = y_train[y_train.shape[0] - to_predict:].reshape(-1,1)

# fill
imp = Imputer(strategy='mean',axis=0).fit(X_train)
X_train = imp.transform(X_train)
X_val = imp.transform(X_val)
X_test = imp.transform(X_test)
# scaler
scaler_x = StandardScaler().fit(X_train)
X_train = scaler_x.transform(X_train)
X_val = scaler_x.transform(X_val)
X_test = scaler_x.transform(X_test)
scaler_y = StandardScaler().fit(y_train)
y_train = scaler_y.transform(y_train)
y_val = scaler_y.transform(y_val)

# shift
window = 12
train_size = X_train.shape[0] + X_val.shape[0]
X = get_previous_values(np.append(np.append(X_train,X_val,axis=0),X_test,axis=0),window)
X_train = X[:X_train.shape[0]]
X_val = X[X_train.shape[0]:X_train.shape[0] + to_predict]
X_test = X[train_size:]

# drop nulls from shift operation
X_train = X_train[window:]
y_train = y_train[window:]

# reshape
X_train = X_train.reshape(X_train.shape[0],window,len(columns))
X_val = X_val.reshape(X_val.shape[0],window,len(columns))
X_test = X_test.reshape(X_test.shape[0],window,len(columns))

# LSTM model
model = Sequential()
model.add(LSTM(100,input_shape=(window, len(columns)),activation='relu'))
model.add(Dense(100,activation='relu'))
model.add(Dense(1))
model.load_weights(results_path+'sj_model.hdf5')
model.compile(loss='mean_absolute_error', optimizer='rmsprop')

predictions = np.round(scaler_y.inverse_transform(model.predict(X_test)))

'''
# fit
model_name = 'sj_model2'
checkpoint = ModelCheckpoint(results_path+model_name+'.hdf5', monitor='val_loss', 
    verbose=1, save_best_only=True, save_weights_only=False,
    mode='auto', period=1)
earlystopping = EarlyStopping(monitor='val_loss', min_delta=0, patience=100,
    verbose=1, mode='auto')

.01835 sj_model
model = Sequential()
model.add(LSTM(100,input_shape=(window, len(columns)),activation='relu'))
model.add(Dense(100,activation='relu'))
model.add(Dense(1))
model.compile(loss='mean_absolute_error', optimizer='rmsprop')

history = model.fit(X_train, y_train, epochs=10000, batch_size=30,
    verbose=2,shuffle=False,validation_data=(X_val,y_val),callbacks=[checkpoint,earlystopping])
model.evaluate(X_val,y_val)
'''

# iq
city = 'iq'
columns = [column for column in df_train.columns if column not in  [target,'city']]
X_train = df_train[df_train['city']==city][columns].values
y_train = df_train[df_train['city']==city][target].values
X_test = df_test[df_test['city']==city][columns].values

df_predictions = df_test[['city','year','weekofyear']].copy()

#to_predict = 48
X_train = X_train[:X_train.shape[0] - to_predict]
y_train = y_train[:y_train.shape[0] - to_predict].reshape(-1,1)
#X_val = X_train[X_train.shape[0] - to_predict:]
#y_val = y_train[y_train.shape[0] - to_predict:].reshape(-1,1)

# fill
imp = Imputer(strategy='mean',axis=0).fit(X_train)
X_train = imp.transform(X_train)
#X_val = imp.transform(X_val)
X_test = imp.transform(X_test)
# scaler
scaler_x = StandardScaler().fit(X_train)
X_train = scaler_x.transform(X_train)
#X_val = scaler_x.transform(X_val)
X_test = scaler_x.transform(X_test)
scaler_y = StandardScaler().fit(y_train)
y_train = scaler_y.transform(y_train)
#y_val = scaler_y.transform(y_val)

# shift
train_size = X_train.shape[0] + X_val.shape[0]
X = get_previous_values(np.append(np.append(X_train,X_val,axis=0),X_test,axis=0),window)
X_train = X[:X_train.shape[0]]
X_val = X[X_train.shape[0]:X_train.shape[0] + to_predict]
X_test = X[train_size:]

# drop nulls from shift operation
X_train = X_train[window:]
y_train = y_train[window:]

# reshape
X_train = X_train.reshape(X_train.shape[0],window,len(columns))
#X_val = X_val.reshape(X_val.shape[0],window,len(columns))
X_test = X_test.reshape(X_test.shape[0],window,len(columns))

# LSTM model
model = Sequential()
model.add(LSTM(100,input_shape=(window, len(columns)),activation='relu',return_sequences=True))
model.add(Dropout(0.1))
model.add(LSTM(100,input_shape=(window, len(columns)),activation='relu',return_sequences=True))
model.add(Dropout(0.1))
model.add(LSTM(100,input_shape=(window, len(columns)),activation='relu',return_sequences=True))
model.add(Dropout(0.1))
model.add(LSTM(100,input_shape=(window, len(columns)),activation='relu',return_sequences=True))
model.add(Dropout(0.1))
model.add(LSTM(100,input_shape=(window, len(columns)),activation='relu'))
model.add(Dropout(0.1))
model.add(Dense(100,activation='relu'))
model.add(Dropout(0.1))
model.add(Dense(100,activation='relu'))
model.add(Dropout(0.1))
model.add(Dense(100,activation='relu'))
model.add(Dropout(0.1))
model.add(Dense(1))

model.load_weights(results_path+'iq_model.hdf5')
model.compile(loss='mean_absolute_error', optimizer='rmsprop')
predictions = np.append(predictions,np.round(scaler_y.inverse_transform(model.predict(X_test))),axis=0)

'''
model_name = 'iq_model2'
checkpoint = ModelCheckpoint(results_path+model_name+'.hdf5', monitor='val_loss', 
    verbose=1, save_best_only=True, save_weights_only=False,
    mode='auto', period=1)
earlystopping = EarlyStopping(monitor='val_loss', min_delta=0, patience=100,
    verbose=1, mode='auto')

iq_model
0.01750292833894491
model = Sequential()
model.add(LSTM(100,input_shape=(window, len(columns)),activation='relu',return_sequences=True))
model.add(Dropout(0.1))
model.add(LSTM(100,input_shape=(window, len(columns)),activation='relu',return_sequences=True))
model.add(Dropout(0.1))
model.add(LSTM(100,input_shape=(window, len(columns)),activation='relu',return_sequences=True))
model.add(Dropout(0.1))
model.add(LSTM(100,input_shape=(window, len(columns)),activation='relu',return_sequences=True))
model.add(Dropout(0.1))
model.add(LSTM(100,input_shape=(window, len(columns)),activation='relu'))
model.add(Dropout(0.1))
model.add(Dense(100,activation='relu'))
model.add(Dropout(0.1))
model.add(Dense(100,activation='relu'))
model.add(Dropout(0.1))
model.add(Dense(100,activation='relu'))
model.add(Dropout(0.1))
model.add(Dense(1))

model.load_weights(results_path+'iq_model.hdf5')
model.compile(loss='mean_absolute_error', optimizer='rmsprop')
model.evaluate(X_val,y_val)




checkpoint = ModelCheckpoint(results_path+model_name+'.hdf5', monitor='val_loss', 
    verbose=1, save_best_only=True, save_weights_only=False,
    mode='auto', period=1)
earlystopping = EarlyStopping(monitor='val_loss', min_delta=0, patience=100,
    verbose=1, mode='auto')

model.fit(X_train, y_train, epochs=1000, batch_size=50,
    verbose=2,shuffle=False)
'''

df_predictions[target] = predictions.astype(int)
df_predictions.to_csv(results_path+"result.csv",index=False)


df_predictions.index = df_test['week_start_date']


import matplotlib.pyplot as plt


city = 'iq'
data = df_train[df_train['city']==city]
pred = df_predictions[df_predictions['city']==city]
plt.plot_date(data.index,data[target],'-',c='slateblue',label='Data')
plt.plot_date(pred.index,pred[target],'-',c='tomato',label='Predictions')
plt.legend()

city = 'sj'
data = df_train[df_train['city']==city]
pred = df_predictions[df_predictions['city']==city]
plt.plot_date(data.index,data[target],'-',c='slateblue',label='Data')
plt.plot_date(pred.index,pred[target],'-',c='tomato',label='Predictions')
plt.legend()


'''__________________________________
def alocate_obcjects(matriz_pos,difficulty):
    bola_pos = alocar_bola_random_canto(matriz_pos)
    buraco_pos = alocar_buraco_canto_oposto(matriz_pos,bola.pos)
    n_obstaculos = int(diff ** 1.5)
    for i in range(n_obstaculos):
        obstaculo = get_random_obstaculo()
        while True:
            if alocar_obstaculo_entre_bola_buraco(matriz_pos,obstaculo,bola_pos,buraco_pos):break
            if alocar_obstaculo_ao_redor(matriz_pos,buraco_pos):break
            if alocar_obstaculo_ao_redor(matriz_pos,bola_pos):break
            if obstaculo.size() > 1:
                obstaculo.size() -= 1
            else:
                print("nao consegue alocar mais objetos")
'''
"""