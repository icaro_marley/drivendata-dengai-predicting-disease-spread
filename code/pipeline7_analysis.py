# -*- coding: utf-8 -*-
"""
Created on Fri Jun  2 11:59:40 2017

@author: R
"""

#split models



# -*- coding: utf-8 -*-

# -*- coding: utf-8 -*-
"""
Created on Sun May 14 00:49:55 2017

@author: icaromarley5
"""


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats
import seaborn as sns
import winsound
from sklearn.externals import joblib
from sklearn.ensemble import RandomForestRegressor, ExtraTreesRegressor, GradientBoostingRegressor
from sklearn import preprocessing
from sklearn.preprocessing import PolynomialFeatures
from sklearn import linear_model
from sklearn.preprocessing import PolynomialFeatures,StandardScaler
from sklearn.model_selection import GridSearchCV, cross_val_score, KFold
from sklearn.metrics import mean_absolute_error
from sklearn.svm import SVR
from sklearn.svm import NuSVR,LinearSVR
from sklearn.ensemble import AdaBoostRegressor,BaggingRegressor
import statsmodels.api as sm
from statsmodels.tsa.arima_model import ARIMA

#np.set_printoptions(threshold='nan')
#pd.set_option('display.max_rows', None)
#preprocess data and returns 1 df
np.set_printoptions(threshold=np.nan)
def pre_process_data(df,response=None,weeks_before=12): 
    #splitting to model the 2 cities
    df = df.copy()

    columns = ['week_start_date']
    df['week_start_date'] = pd.to_datetime(df['week_start_date'])
    df.set_index('week_start_date',inplace=True)
    #df.drop(columns,axis=1,inplace=True)
    

    
    columns = df.columns.values
    if response != None:
        columns = np.delete(columns,df.columns.get_loc(response))
    columns = np.delete(columns,df.columns.get_loc('city'))
        
    #print(columns)
    old_len_columns = len(columns) 

    #split the dfs
    df_train_iq = df[df['city'] == 'iq'].copy()
    df_train_sj = df[df['city'] == 'sj'].copy()    
    dfs_train = [df_train_iq,df_train_sj]
    

    
    for df in dfs_train:
        #dealing with na values 
        #df.fillna(method='ffill',inplace=True)
        df.drop('city',axis=1,inplace=True)   
        df.interpolate(inplace=True)
        
        for n in range(1,weeks_before+1):
            for column in columns:
                df[column+"_"+ str(n)] = df[column].shift(n)
    
        #dealing with na values for the shift operation
        df.fillna(method='bfill',inplace=True)#columns shifted 
    
        

        
    return dfs_train,old_len_columns


#reading training files
path = "../modified data/"
features_path = path + "dengue_features_train.csv"
labels_path = path + "dengue_labels_train.csv"

df_feat = pd.read_csv(features_path)
df_lab = pd.read_csv(labels_path)

#merge
df_merge = pd.merge(df_feat,df_lab,on=["city","year","weekofyear"])


#selecting response
response = "total_cases"

dfs_train,old_len_columns = pre_process_data(df_merge,response)


'''
#hist plot
f, axarr = plt.subplots(1,2,sharex=True,sharey=True,figsize=(7,4))
index = 0 
for city,df_train in zip(["Iquitos","San Juan"],dfs_train):
    f.subplots_adjust(hspace=.3)
    axarr[index].hist(df_train[response], 50)
    axarr[index].set_title(city)
    index+=1
plt.suptitle("Histogram of total dengue fever cases")
plt.show()


#line plot
f, axarr = plt.subplots(1,2,sharey=True,sharex=True,figsize=(7,4))
index = 0 
for city,df_train in zip(["Iquitos","San Juan"],dfs_train):
    #df_train.set_index(['weekofyear','year'],inplace=True)
    f.subplots_adjust(hspace=.3)
    axarr[index].plot(df_train[response])
    axarr[index].set_title(city)
    index+=1
    #df_train.reset_index()
plt.suptitle("Line Plot of total dengue fever cases")
plt.show()

#correlations
for city,df_train in zip(["Iquitos","San Juan"],dfs_train):
    corrmat = df_train.corr()
    fig = plt.figure(figsize=(50, 10), dpi=100)
    ticks = np.arange(len(corrmat['total_cases']))
    plt.plot(ticks,corrmat['total_cases'])
    labels = corrmat['total_cases'].index.values
    plt.xticks(ticks, labels, rotation='vertical')
    plt.title(city)
    plt.show()

for method in ['pearson','spearman','kendall']:
    print(method.upper())
    for df_train,name in zip(dfs_train,['IQUITOS','SAN JUAN']):
        print(name)
        corr = df_train.corr(method=method)['total_cases']
        corr.drop('total_cases',inplace=True)
        print(corr.sort_values(ascending = False)[:50])
    print()

     


#usar para hexbin tbm
#scatter plot predictor x reponse by medition week 
len_side = 6
predictors = np.delete(df_train.columns,df_train.columns.get_loc(response))
predictors_len = len(predictors)

for city,df_train in zip([["Iquitos","San Juan"][1]],[dfs_train[1]]):
    df_train = df_train.copy()
    step = old_len_columns
    limit_inf = 0
    limit_max = step
    week = 0
    while (limit_max<=predictors_len):
        index = 0
        side = 0
        selected = predictors[limit_inf:limit_max]
        
        f, axarr = plt.subplots(int(len(selected)/len_side) + 1,len_side,
                                figsize=(19,14),sharey=True)
        f.subplots_adjust(hspace=.5)
        for predictor in selected:
            x = df_train[predictor]#.apply(lambda x:np.log(x))
            y = df_train[response].apply(lambda x: np.sqrt(x))
            df_train[predictor] = x
            df_train[response] = y
            #print(predictor,index,side)
            axarr[index,side].scatter(x,y)#,cmap='inferno')
            axarr[index,side].set_xlabel(predictor)
            corr = scipy.stats.pearsonr(x,y)
            axarr[index,side].set_title("Pearson R {:.2f} P {:.2f}".format(corr[0],corr[1]))
            side+=1
            if side > len_side - 1:
                side = 0
                index+=1   
        plt.suptitle(city + ": total dengue fever cases by predictors week " + str(week))
        plt.show()  
        
        #print(limit_inf,limit_max,selected,step)
        limit_inf = limit_max
        limit_max += step
        week+= -1
        
    for method in ['pearson','spearman','kendall']:
        print(method.upper())
        print(city)
        corr = df_train.corr(method=method)['total_cases']
        corr.drop('total_cases',inplace=True)
        print(corr.sort_values(ascending = False)[:20])
        print()
        
'''



#TRAINING
def train(x,y,estimator,params):
    outer_cv = KFold(n_splits=10, shuffle=False)
    inner_cv = KFold(n_splits=5, shuffle=False)
    
    #non nested
    clf = GridSearchCV(estimator, params,scoring="neg_mean_absolute_error",cv=inner_cv)#,verbose=10000000000)
    clf.fit(x,y)
    tab = "   "
    print('non nested CV')
    print(tab,'best params',clf.best_params_)
    print(tab,"score",clf.best_score_)
    print(tab,"std",clf.cv_results_['std_test_score'])
    model = clf.best_estimator_
    pred = model.predict(x)
    true = y
    print(tab,"full dataset score", - mean_absolute_error(true,pred))

    """
    #nested
    clf = GridSearchCV(estimator, params,scoring="neg_mean_absolute_error",cv=inner_cv)
    nested_cv = cross_val_score(clf, X=x, y=y, cv=outer_cv)
    print('nested CV')
    print(tab,"score",nested_cv.mean())
    print(tab,"std",nested_cv.std())
    """
    return model

'''
IQUITOS 

'''

print('\nIQUITOS MODEL TRAINING\n')
df_train = dfs_train[0].copy()
def back_transform_iq_y(values):
    return values.round(0).astype(int)


#return x
def pre_process_iq(df,get_response=True):
    explanatory = [
    'reanalysis_specific_humidity_g_per_kg_3',
    'weekofyear_10',
    ]
    
    x = df[explanatory].copy()
    #scaler = preprocessing.StandardScaler().fit(x)
    #x = scaler.transform(x) 
    #poly = PolynomialFeatures(degree=3)
    #x = poly.fit_transform(x)
    
    if get_response:
        y = df[response].copy()
        return x,y

    return x



x_iq,y_iq = pre_process_iq(df_train)
x = x_iq
y = y_iq


#estimator = RandomForestRegressor(random_state=100,n_jobs = -1)
#-6.75011223048 on reanalysis_min_air_temp_k well fitted 



#estimator = linear_model.LinearRegression()
#-6.09716923523     'weekofyear_11','weekofyear_12','weekofyear_8','reanalysis_specific_humidity_g_per_kg_3','reanalysis_dew_point_temp_k',

#estimator = GradientBoostingRegressor(random_state=100, n_estimators=12)
#-6.18784649128 reanalysis_min_air_temp_k weekofyear_10 (lower overfitting)



estimator = SVR()
#-5.60065916859  on weekofyear_10 reanalysis_specific_humidity_g_per_kg_3 kernel rbf


params = {

}

# Non_nested parameter search and scoring
best_iq_model = train(x,y,estimator,params)






'''
SAN JUAN 
'''




'''
np.set_printoptions(precision=4)


X = df_train[['ndvi_nw_3']]
Y = np.sqrt(df_train[response].copy())
#from numpy import inf
#Y[Y == -inf] = np.NaN

#Y = pd.DataFrame(Y).interpolate().values[:,0]
estimator = RandomForestRegressor(random_state=100,n_estimators=100)
estimator.fit(X,Y)
best_sj_model = estimator
predictions = estimator.predict(X)

plt.scatter(X,Y)
plt.scatter(X,predictions,c='r')
plt.show()
print(mean_absolute_error(Y,predictions))

plt.scatter(X,df_train[response].copy(),c='blue')
plt.scatter(X,np.power(predictions,2),c='red')



#Y = np.power(np.sqrt(df_train[response].copy()),2)

rfr 30.9087
lr 27.8534




#plot line plot from predictions 
#and real values
fig = plt.figure(figsize=(7, 5), dpi=100)
y_true = df_train['total_cases']
sj_pred = back_transform_sj_y(best_sj_model.predict(X))
ticks = np.arange(len(y_true))
y_pred = sj_pred
plt.plot(ticks,y_true,c='b',label = "Real Values")
plt.plot(ticks,sj_pred,c='r',label = "Predicts")
plt.legend()
plt.show()

#onde está a previsao de 200 do primeiro plot? (scatter)
np.power(predictions,2).max()
sj_pred.max()

back_transform_sj_y(predictions).max()

print(mean_absolute_error(y_true,sj_pred))

#something wrong with train?

'''


'''
sqrt on y pearson

'weekofyear'??


reanalysis_tdtr_k_9 99 0.00777166842761
station_diur_temp_rng_c_11 98 0.00781015454123
reanalysis_sat_precip_amt_mm_2 98 0.00327332621083
precipitation_amt_mm_2 98 0.00327332621083
station_precip_mm_3 92 0.00615757859187
ndvi_nw_3  99  0.00239316239316
ndvi_se 95 0.00425943732194



'''


print('\nSAN JUAN MODEL TRAINING\n')
df_train = dfs_train[1].copy()
def back_transform_sj_y(values):
    return np.power(values,2).round(0).astype(int)

#return x
def pre_process_sj(df,get_response=True):
    explanatory = [
 'station_min_temp_c_10',
 'station_avg_temp_c_12',
 'station_min_temp_c_8',
 'station_min_temp_c_7',
 'station_avg_temp_c_5',
     ]
    
    
    explanatory = [
     'station_avg_temp_c_12',
     'station_min_temp_c_8',
     'station_min_temp_c_7',
     'station_avg_temp_c_5', #       
    
    'station_min_temp_c_10',
    #'station_max_temp_c_11',
    #'reanalysis_specific_humidity_g_per_kg',
    'reanalysis_dew_point_temp_k',#
    ]
    
    
    explanatory = [
    'ndvi_nw_3'
    ]

    
    
    x = df[explanatory].copy()

    #scaler = preprocessing.StandardScaler().fit(x)
    #x = scaler.transform(x) 
    '''
    poly = PolynomialFeatures(degree=6)
    x = poly.fit_transform(x)
    '''
    if get_response:
        y =np.sqrt(df[response].copy())
        return x,y
    return x


x_sj,y_sj = pre_process_sj(df_train)
x = x_sj
y = y_sj


estimator = GradientBoostingRegressor(random_state=100,n_estimators=25,max_depth=1,presort=False,criterion='mae')
estimator = RandomForestRegressor(random_state=100,n_estimators=100)

#estimator = SVR(kernel='rbf')#nice   
#estimator = SVR(kernel='linear')#nice     

#estimator = NuSVR() 
               
#estimator = LinearSVR(random_state=100,max_iter=4000)

#estimator = AdaBoostRegressor(estimator,random_state=100,n_estimators=500)

#estimator = RandomForestRegressor(random_state=100,n_jobs = -1,n_estimators=50) #ovefitting or underfitting

                                  
#estimator = RandomForestRegressor(random_state=100,n_jobs = -1,n_estimators=i) #ovefitting or underfitting
#estimator = GradientBoostingRegressor(random_state=100) #overfitting

estimator = linear_model.LinearRegression()  #high bias
#estimator = GradientBoostingRegressor(random_state=100) #overfitting



params = {
#'n_estimators':np.append(np.arange(50,100),25)
#'max_depth':np.arange(1,10)
#'random_state':np.arange(1,100)

}

best_sj_model = train(x,y,estimator,params)




'''
#time series analyis: decomposition
y = pd.DataFrame(y).astype('float64')
freq = 52
decomposition = sm.tsa.seasonal_decompose(y.values,freq=freq, model='additive')
fig = decomposition.plot()
plt.show()



trend = pd.DataFrame(decomposition.trend).dropna()
seasonal = pd.DataFrame(decomposition.seasonal)
residuals = pd.DataFrame(decomposition.resid).dropna()
'''



'''
from numpy import polyfit
X = x
y_ = y.values
degree = 2
coef = polyfit(X, y_, degree)
print('Coefficients: %s' % coef)
# create curve
curve = list()
for i in range(len(X)):
	value = coef[-1]
	for d in range(degree):
		value += X[i]**(degree-d) * coef[d]
	curve.append(value)
# plot curve over original data
plt.plot(y_)
plt.plot(curve, color='red', linewidth=3)
plt.show()
'''

'''
regressao linear com degrau e week of year
'''



'''
#plot line plot from predictions 
#and real values
fig = plt.figure(figsize=(7, 5), dpi=100)
y_true = df_train['total_cases']
sj_pred = back_transform_sj_y(best_sj_model.predict(x_sj))
ticks = np.arange(len(y_true))
y_pred = sj_pred
plt.plot(ticks,y_true,c='b',label = "Real Values")
plt.plot(ticks,y_pred,c='r',label = "Predicts")
plt.legend()
plt.show()
'''




'''
there is no trend
there is some seasonallity
'''
#models
'''
#TREND
trend = pd.DataFrame(decomposition.trend).fillna(method='bfill').fillna(method='ffill')
y = trend.values[:,0]
estimator = GradientBoostingRegressor(random_state=100,max_depth=10) #overfitting
params = {
#'n_estimators':np.append(np.arange(50,100),25)
#'max_depth':np.arange(1,10)
#'random_state':np.arange(1,100)
}
season_model = train(x,y,estimator,params)
predictions = season_model.predict(x)
plt.plot(y,c='b', label = "Real Values")
plt.plot(predictions,c='r', label = "Predicts")
plt.show()
trend.plot()
'''

'''
df_train['seasonal'] = y
corr = df_train.corr(method='pearson')['seasonal']

corr.sort_values(ascending = False)
'''






'''
#SJ SEASONAL MODEL (overfit model)
explanatory = [
    'reanalysis_specific_humidity_g_per_kg_7'  ,
    'weekofyear'
    ]
y = seasonal.values[:,0]
estimator = GradientBoostingRegressor(random_state=100,max_depth=10) #overfitting
params = {
#'n_estimators':np.append(np.arange(50,100),25)
#'max_depth':np.arange(1,10)
#'random_state':np.arange(1,100)
}
season_model = train(x,y,estimator,params)
predictions = season_model.predict(x)
plt.plot(y,c='b', label = "Real Values")
plt.plot(predictions,c='r', label = "Predicts")
plt.show()
best_sj_model = season_model


#season adjust test
season_series = y + np.abs(np.min(y))
plt.plot(df_train[response].values,c='b',label='total cases')   
plt.plot(season_series,c='r',label='season')     
plt.plot((df_train[response] - season_series).values,c='green',label='season stationary')
plt.legend()
plt.show()
'''

'''
df_train['seasonal'] = y
corr = df_train.corr(method='pearson')['seasonal']

corr.sort_values(ascending = False)
'''

'''
#plot line plot from predictions 
#and real values
fig = plt.figure(figsize=(7, 5), dpi=100)
y_true = dfs_train[1]['total_cases'].copy().append(dfs_train[0]['total_cases'])
sj_pred = back_transform_sj_y(best_sj_model.predict(x_sj))
iq_pred = back_transform_iq_y(best_iq_model.predict(x_iq))
ticks = np.arange(len(y_true))
y_pred = np.concatenate([sj_pred,iq_pred])
plt.plot(ticks,y_true,c='b',label = "Real Values")
plt.plot(ticks,y_pred,c='r',label = "Predicts")
plt.legend()
plt.show()
'''



