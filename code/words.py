# -*- coding: utf-8 -*-
#@author: Ícaro Oliveira

def count_words(text):
    to_replace = [",", "\n", ".", "\t"]
    for replace_str in to_replace:
        text = text.replace(replace_str, " ")
    
    words = [word for word in text.split(" ") if word != ""]
    #print(words)
    return len(words)


full_text = """
Introduction
	The purpose of this study was to apply the techniques learned through the Coursera’s Data Analysis and Interpretation specialization by the Wesleyan University. In the capstone project, the students were asked to choose a data set and a research question and report the process systematically. Some of the data sets available for this work were from Industry Partners of the specialization and Data Science competitions, such as DrivenData and Kaggle.
	Data Science competitions are hosted on the Internet to challenge professionals and freelancers in the area across the globe. Most of them have deadlines, rankings, prizes and allow team entries to stimulate the competitors and their cooperation. The process is quite simple: they provide train and test data (the latter doesn’t have values in the predictor variable) along with a summary or code book describing the problem to be solved, the explanatory and response variables, and then compute a selected metric (or metrics) from results submitted and rank those values.
	In this subject, DrivenData is an interesting option since they host complex competitions focused on solving real world problems and the winning models in those competitions have higher probability of being used to improve predictions and forecasting in the problems.
At the time that students enrolled the capstone project (April/2017) , DrivenData hosted the competitions “Warm Up: Predict Blood Donations”, to improve predictions of blood donations, “Pump it Up: Data Mining the Water Table”, focused on predicting faulty water pumps in Tanzania, and “DengAI: Predicting Disease Spread”, which impels the competitors to predict dengue fever cases in Peru.
I choose the latter competition because I live in Brazil, a latin country (as Peru) which suffers from yearly dengue fever outbreaks that kills hundreds of people, forcing the government to spend millions of dollars in campaigns against the Aedes Aegypti.
The data set provided has measurements by week in two different peruvian cities: Iquitos and San Juan and a very good model for this problem can help the Peru’s Government to control the amount of aid (supplies, medics, nurses and so on) needed by each city in each week and maybe even help other governments create its own models to predict the outbreaks.
Since that is an advanced Time Series problem, it asks for different and more complexes analysis and forecasts techniques from Time Series Analysis (TSA), which weren't taught in the specialization. Also, in this special kind of problem, most of the individual regression (linear and nonlinear) tools usually doesn’t work well since they assume independence between the response variable values.
	Considering the deadline of this report (June/2017) and the complexity to learn some of advanced methods of Time Series Analysis and produce a good and significant forecasting model, I choose a more simple approach: use the regression tools combined with other insights about the problem to get better results. So, the main purpose of this study was to identify the best predictors associated to dengue fever cases from multiple predictors such as temperature, humidity and precipitation provided by the competition.


Methods
Since in Time Series the response variable is strongly correlated with itself, is better to use all data obtained. Validations methods doesn’t work well for those problems because the order of the data matters and it varies significantly with time.
The train sample provided has was 1456 rows that measures total dengue fever counts in San Juan and Iquitos by year and week. San Juan has 936 measured between and April/1990 and April/2008 and Iquitos has 520 collected in the timespan of July/2000 and June/2010. As many forecasting problems, the test data contain values from subsequent dates: has values in the range of April/2008 to April/2013 for San Juan and of July/2010 to June/2013 for Iquitos.
The data comes from multiples sources:
(From DrivenData’s website) “Dengue surveillance data is provided by the U.S. Centers for Disease Control and prevention, as well as the Department of Defense’s Naval Medical Research Unit 6 and the Armed Forces Health Surveillance Center, in collaboration with the Peruvian government and U.S. universities. Environmental and climate data is provided by the National Oceanic and Atmospheric Administration (NOAA), an agency of the U.S. Department of Commerce.“
A simple line and histogram plot shows that the response variable behaves differently for the two cities:Figure 1. Histogram of total dengue fever cases (50 bin size)
Figure 2. Line plot of total dengue fever cases
In general, San Juan has strong seasonality pattern between the response values with very high peaks and Iquitos has fewer and lower peaks with a more behaved pattern. 
The explanatory variables are all continuous and measures climatic related factors:
Maximum, minimum and average temperature, total precipitation (amount of rain) and diurnal temperature range, measured by NOAA's GHCN daily climate data weather station;
total precipitation, collected by PERSIANN satellite precipitation measurements and
total precipitation, average, maximum and minimum air temperature, mean dew point temperature, specific humidity (ratio of the mass of water vapor to the total mass of the moist air) and mean relative humidity (current humidity relative to the maximum for that temperature) and diurnal temperature range, from NOAA's NCEP Climate Forecast System Reanalysis.
Considering the length of the cycle of life of the mosquito (usually more than 1 month) and assuming strong direct association between the pest population and disease cases, values from past weeks (up until 12 weeks before) of those variables were also considered in the analysis. Nonvalid values in posterior variables in the first 12 weeks resulted from this operation were filled with the values from next weeks (backward filling). Also in regard of that assumption, original nonvalid values were dealt by propagating the values from past weeks (forward filling).
The analysis tools used were measurements of central tendency (mean) and spread (standard deviation) to compare the behavior of the response variable on both cities, diagrams (scatter and hexagonal binning plots), Pearson’s correlation test to visualize and test the significance of a linear correlation between an explanatory and the response variable, Lasso regression to test correlation using more than one explanatory variable and feature selection, and data transformation (by applying logarithmic transformation on the response variable) to improve linear correlation and allow the use of linear regression methods. 
 

"""

ignore_text = """
(From DrivenData’s website) “Dengue surveillance data is provided by the U.S. Centers for Disease Control and prevention, as well as the Department of Defense’s Naval Medical Research Unit 6 and the Armed Forces Health Surveillance Center, in collaboration with the Peruvian government and U.S. universities. Environmental and climate data is provided by the National Oceanic and Atmospheric Administration (NOAA), an agency of the U.S. Department of Commerce.“

Figure 1. Histogram of total dengue fever cases (50 bin size)
Figure 2. Line plot of total dengue fever cases

Methods

Introduction


"""


n_words = count_words(full_text)

number_ignore_words = count_words(ignore_text)

print("word count:", n_words - number_ignore_words)


