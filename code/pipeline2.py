# -*- coding: utf-8 -*-
"""
Created on Sun Apr 30 16:04:15 2017

@author: icaromarley5
"""
'''
-find best random state, see if fit again resets model
--usar bagging/boosting
--gridsearchcv
--improve randomstatemodel com fit e refit


pipeline sklearn
time series, 
pgm
 ver data transformation ,
 fill na com valores próxima ,
''' 

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import GridSearchCV
from sklearn.svm import SVR
import scipy.stats
import seaborn as sns
import winsound

#reading training files
path = "../modified data/"
features_path = path + "dengue_features_train.csv"
labels_path = path + "dengue_labels_train.csv"

df_feat = pd.read_csv(features_path)
df_lab = pd.read_csv(labels_path)

#merge
df_merge = pd.merge(df_feat,df_lab,on=["city","year","weekofyear"])

#preprocess data and returns 2 dataframes with Iquitos and San Juan Data
def pre_process_data(df,final=False,weeks_before=2):
    #dropping unwanted columns 
    #columns = ['week_start_date']
    #df.drop(columns,axis=1,inplace=True)
    
    
    #splitting to model the 2 cities
    df_iq = df[df['city'] == 'iq'].copy()
    df_sj = df[df['city'] == 'sj'].copy()
    
    
    #selecting variables
    response = "total_cases"
  
        
    predictors = df.columns.values
    #predictors = np.delete(predictors,-1)
    #predictors = np.delete(predictors,[0,1])

    df_train_iq = df_iq[list(predictors)].copy()
    df_train_sj = df_sj[list(predictors)].copy()
    if not final:
        df_train_iq[response] = df_iq[response].copy()
        df_train_sj[response] = df_sj[response].copy()
    
    #getting values from past weeks
    dfs_train = [df_train_iq,df_train_sj]
    old_len_predictors = len(predictors)
    columns = df_train_sj.columns.values

    for n in range(1,weeks_before+1):
         for df_train in dfs_train:  
             for column in columns:
                 df_train[column+"_"+ str(n)] = df_train[column].shift(n)
    
    predictors = np.delete(df_train_sj.columns.values,old_len_predictors)
    
    
    #dealing with na values -> fill with mean
    for df_train in dfs_train:
        df_train.fillna(df_train.mean(),inplace=True)

    return dfs_train,response,old_len_predictors

dfs_train,response,old_len_predictors = pre_process_data(df_merge)
'''
#hist plot
f, axarr = plt.subplots(1,2,sharex=True,sharey=True,figsize=(7,4))
index = 0 
for city,df_train in zip(["Iquitos","San Juan"],dfs_train):
    f.subplots_adjust(hspace=.3)
    axarr[index].hist(df_train[response], 50)
    axarr[index].set_title(city)
    index+=1
plt.suptitle("Histogram of total dengue fever cases")
plt.show()

#line plot
f, axarr = plt.subplots(1,2,sharey=True,figsize=(7,4))
index = 0 
for city,df_train in zip(["Iquitos","San Juan"],dfs_train):
    f.subplots_adjust(hspace=.3)
    axarr[index].plot(df_train[response])
    axarr[index].set_title(city)
    index+=1
plt.suptitle("Line Plot of total dengue fever cases")
plt.show()

#correlations

for city,df_train in zip(["Iquitos","San Juan"],dfs_train):
    corrmat = df_train.corr()
    
    fig = plt.figure(figsize=(26, 10), dpi=100)
    ticks = np.arange(len(corrmat['total_cases']))
    plt.plot(ticks,corrmat['total_cases'])
    labels = corrmat['total_cases'].index.values
    plt.xticks(ticks, labels, rotation='vertical')
    plt.title(city)
    plt.show()


#scatter plot predictor x reponse by medition week 
len_side = 6


predictors_len = len(predictors)
for city,df_train in zip(["Iquitos","San Juan"],dfs_train):
    df_train.dropna(inplace=True)
    step = old_len_predictors
    limit_inf = 0
    limit_max = step
    week = 0
    while (limit_max<=predictors_len):
        index = 0
        side = 0
        selected = predictors[limit_inf:limit_max]
        
        f, axarr = plt.subplots(int(len(selected)/len_side) + 1,len_side,
                                figsize=(19,14),sharey=True)
        f.subplots_adjust(hspace=.5)
        for predictor in selected:
            x = df_train[[predictor]]
            y = df_train[response]
            #print(predictor,index,side)
            axarr[index,side].scatter(x,y,c='b')
            axarr[index,side].set_xlabel(predictor)
            corr = scipy.stats.pearsonr(x,y)
            axarr[index,side].set_title("Pearson R {:.2f} P {:.2f}".format(corr[0],corr[1]))
            side+=1
            if side > len_side - 1:
                side = 0
                index+=1   
        plt.suptitle(city + ": total dengue fever cases by predictors week " + str(week))
        plt.show()  
        
        #print(limit_inf,limit_max,selected,step)
        limit_inf = limit_max
        limit_max += step
        if limit_inf >= step and week == 0:
            step+=1
            first_week = True
            limit_max+=1
        week+= -1
'''



'''
ANALYSIS

iquitos
few outliers 
linear regression:
totalcase 1 good variable


san juan 
many outliers 
linear regression
totalcase_1 great variable
'''

#train models with gridsearchcv
def train(x,y,estimator,params):
    clf = GridSearchCV(estimator, params,scoring="neg_mean_absolute_error",cv=10,verbose=10000000000)
    clf.fit(x,y)
    print("best score",clf.best_score_)
    print("best params",clf.best_params_)
    winsound.Beep(300,300)
    return clf.best_estimator_  

def train_final(x,y,best_model):
    best_model = SVR(**best_model.get_params())
    best_model.fit(x,y)
    return best_model
    
    
#SAN JUAN MODEL
#svr linear  -7.90444788185  
df_train =  dfs_train[1]
explanatory_sj = ['total_cases_1']
x = df_train[explanatory_sj]
y = df_train[response]
estimator = SVR(kernel="linear",epsilon=0.076999999999999999,C=10.900000000000009)
params = {}
best_sj_model = train(x,y,estimator,params)
best_sj_model = train_final(x,y,best_sj_model)


#IQUITOS MODEL
#svr linear  -3.68641950417
df_train = dfs_train[0]

explanatory_iq = ['total_cases_1','total_cases_2']
x = df_train[explanatory_iq]
y = df_train[response]
estimator = SVR(kernel='linear',C=4, epsilon = 0.089999999999999997,tol=0.00092000000000000014)
params = {}
best_iq_model = train(x,y,estimator,params)
best_iq_model = train_final(x,y,best_iq_model)



#reading dfs
features_path2 = path + "dengue_features_test.csv"
df_test = pd.read_csv(features_path2)
dfs_test,response,old_len_predictors = pre_process_data(df_test,final=True)

#predictions
#para cada modelo

for df_test,best_model,x in zip(dfs_test,[best_iq_model,best_sj_model],[explanatory_iq,explanatory_sj]):
    print(df_test.columns)
    df_test['total_cases'] = best_model.predict(df_test[x])

#SAVING SUMISSION FILE
result_path = "result/"
df_result.to_csv(result_path+"result.csv",index=False)

#saving
#para cada modelo
#from sklearn.externals import joblib
#model_path = "models/"
#joblib.dump(estimator, model_path+ 'SECONDSUB_RFR.pkl') 
#clf = joblib.load('filename.pkl')

