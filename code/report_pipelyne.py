# -*- coding: utf-8 -*-
"""
Created on Sun Jun 11 20:13:28 2017

@author: icaromarley5
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats
import seaborn as sns
import winsound
from sklearn.externals import joblib
from sklearn.ensemble import RandomForestRegressor, ExtraTreesRegressor, GradientBoostingRegressor
from sklearn import preprocessing
from sklearn.preprocessing import PolynomialFeatures
from sklearn import linear_model
from sklearn.preprocessing import PolynomialFeatures,StandardScaler
from sklearn.model_selection import GridSearchCV, cross_val_score, KFold
from sklearn.metrics import mean_absolute_error
from sklearn.svm import SVR
from sklearn.svm import NuSVR,LinearSVR
from sklearn.ensemble import AdaBoostRegressor,BaggingRegressor
import statsmodels.api as sm
from statsmodels.tsa.arima_model import ARIMA

#np.set_printoptions(threshold='nan')
#pd.set_option('display.max_rows', None)
#preprocess data and returns 1 df
np.set_printoptions(threshold=np.nan)
def pre_process_data(df,response=None,weeks_before=12): 
    #splitting to model the 2 cities
    df = df.copy()

    columns = ['week_start_date']
    df['week_start_date'] = pd.to_datetime(df['week_start_date'])
    df.set_index('week_start_date',inplace=True)
    #df.drop(columns,axis=1,inplace=True)
    

    
    columns = df.columns.values
    if response != None:
        columns = np.delete(columns,df.columns.get_loc(response))
    columns = np.delete(columns,df.columns.get_loc('city'))
        
    #print(columns)
    old_len_columns = len(columns) 

    #split the dfs
    df_train_iq = df[df['city'] == 'iq'].copy()
    df_train_sj = df[df['city'] == 'sj'].copy()    
    df_train_iq.drop('city',axis=1,inplace=True)
    df_train_sj.drop('city',axis=1,inplace=True)    
    
    dfs_train = [df_train_iq,df_train_sj]
    
    for df in dfs_train:
        #dealing with na values 
        df.fillna(method='ffill',inplace=True) 
        
        for n in range(1,weeks_before+1):
            for column in columns:
                df[column+"_"+ str(n)] = df[column].shift(n)
    
        #dealing with na values for the shift operation
        df.fillna(method='bfill',inplace=True)#columns shifted 
    
        

        
    return dfs_train,old_len_columns


#reading training files
path = "../modified data/"
features_path = path + "dengue_features_train.csv"
labels_path = path + "dengue_labels_train.csv"

df_feat = pd.read_csv(features_path)
df_lab = pd.read_csv(labels_path)

#merge
df_merge = pd.merge(df_feat,df_lab,on=["city","year","weekofyear"])


#selecting response
response = "total_cases"

dfs_train,old_len_columns = pre_process_data(df_merge,response)

'''	
pd.options.display.float_format = '{:,.2f}'.format
for city,df_train in zip(["Iquitos","San Juan"],dfs_train):
    print(city)
    df_train["Total dengue fever cases"] = df_train[response]
    print(df_train[["Total dengue fever cases"]].describe())

#hist plot
f, axarr = plt.subplots(1,2,sharex=True,sharey=True,figsize=(7,4))
index = 0 
for city,df_train in zip(["Iquitos","San Juan"],dfs_train):
    f.subplots_adjust(hspace=.3)
    axarr[index].hist(df_train[response], 50)
    axarr[index].set_title(city)
    index+=1
plt.suptitle("Histogram of total dengue fever cases")
plt.show()


#line plot
f, axarr = plt.subplots(1,2,sharey=True,sharex=True,figsize=(7,4))
index = 0 
for city,df_train in zip(["Iquitos","San Juan"],dfs_train):
    #df_train.set_index(['weekofyear','year'],inplace=True)
    f.subplots_adjust(hspace=.3)
    axarr[index].plot(df_train[response])
    axarr[index].set_title(city)
    index+=1
    #df_train.reset_index()
plt.suptitle("Line Plot of total dengue fever cases")
plt.show()


#correlations
for city,df_train in zip(["Iquitos","San Juan"],dfs_train):
    #df_train = df_train.apply(lambda x: np.sqrt(x))
    #df_train[response] =  df_train[response].apply(lambda x: np.log(x))
    corrmat = df_train.corr()["total_cases"]
    fig = plt.figure(figsize=(50, 10), dpi=100)
    ticks = np.arange(len(corrmat))
    plt.plot(ticks,corrmat)
    labels = corrmat.index.values
    plt.xticks(ticks, labels, rotation='vertical')
    plt.title(city)
    plt.show()
    print("Pearson correlation coefficient", city)
    print(corrmat.sort_values(ascending = False)[:10])
    print()
'''

#TRAINING
def train(x, y, estimator,back_transform):
    estimator.fit(x, y)
    pred = back_transform(estimator.predict(x))
    true = y
    print("    full dataset score", - mean_absolute_error(true, pred))
    return estimator


'''
IQUITOS 



sqrt y
reanalysis_specific_humidity_g_per_kg      0.324761
reanalysis_dew_point_temp_k                0.316092
year                                       0.314857
reanalysis_min_air_temp_k_5                0.270045
station_min_temp_c_1                       0.269903




log1p y

year                                      0.35
reanalysis_specific_humidity_g_per_kg_3   0.35
reanalysis_dew_point_temp_k_3             0.34
reanalysis_min_air_temp_k_3               0.29

'''


print('\nIQUITOS MODEL TRAINING\n')
df_train = dfs_train[0]

def back_transform_iq_y(values,scaler=None):
    #return values.round(0).astype(int)
    return np.power(values,2).round(0).astype(int)


#return x
def pre_process_iq(df,get_response=True,scaler = None):
    explanatory = [
'year',
'reanalysis_specific_humidity_g_per_kg_3',
'reanalysis_dew_point_temp_k_3',
'reanalysis_min_air_temp_k_3'
    ]

    #explanatory = df.columns
    x = df[explanatory].copy()#.apply(lambda x: np.log1p(x))
    x = x.values
    if scaler == None:
        scaler = preprocessing.StandardScaler().fit(x)
    x = scaler.transform(x) 

    x = pd.DataFrame(x,columns = [
    "Year",
    "(reanalysis) Specific humidity",
    "(reanalysis) Dew point temperature",
    "(reanalysis) Minimum air temperature", 
     ])
    if get_response:
        y = df[response].copy().apply(lambda x: np.sqrt(x))
        y = y.replace({-np.Inf: np.NaN})
        y = y.interpolate()
        return x,y,scaler

    return x,scaler



x_iq,y_iq,scaler = pre_process_iq(df_train)##########ALGO TA ACONTECENDO AQUI
x = x_iq
y = y_iq




'''
#correlations
x[response] = y
#df_train = df_train.apply(lambda x: np.sqrt(x))
#df_train[response] =  df_train[response].apply(lambda x: np.log(x))
corrmat = x.corr()[response]
print("Pearson correlation coefficient")
print(corrmat.sort_values(ascending = False)[:30])
'''


'''
#usar para hexbin tbm
#scatter plot predictor x reponse 
len_side = 2
f, axarr = plt.subplots(int(len(x.columns)/len_side) + 1,len_side,
                        figsize=(13,8),sharey=True)

f.subplots_adjust(hspace=.3)
f.subplots_adjust(wspace=.1)
side = 0
index = 0
for predictor in x.columns:
    x_ = x[predictor].values
    y_ = y.values
    #axarr[index,side].scatter(x_,y_)
    hb = axarr[index,side].hexbin(x_,y_,bins=50,cmap="OrRd")#hexbin cmap='inferno')
    cb = f.colorbar(hb, ax=axarr[index,side])
    axarr[index,side].set_xlabel(predictor)
    side += 1
    if side > len_side - 1:
        side = 0
        index+=1   
plt.suptitle("Total dengue fever cases by predictor")
plt.show()  
'''

estimator = linear_model.Lasso(alpha=.1)

best_iq_model = train(x,y,estimator,back_transform_iq_y)

for name,coeff in zip(x.columns,best_iq_model.coef_):
    print(name,coeff)



'''
SAN JUAN 

log y (tentar outro dia?)

station_avg_temp_c_12                      0.57
station_avg_temp_c_11                      0.57
station_avg_temp_c_10                      0.56
station_avg_temp_c_9                       0.55
reanalysis_dew_point_temp_k_9              0.53
reanalysis_specific_humidity_g_per_kg_9    0.53
station_avg_temp_c_8                       0.53
station_min_temp_c_10                      0.53
station_min_temp_c_11                      0.53
reanalysis_specific_humidity_g_per_kg_8    0.53
reanalysis_dew_point_temp_k_8              0.53
reanalysis_dew_point_temp_k_10             0.53
reanalysis_specific_humidity_g_per_kg_10   0.53
station_min_temp_c_9                       0.53
station_min_temp_c_12                      0.52
station_max_temp_c_12                      0.52
reanalysis_dew_point_temp_k_11             0.51
reanalysis_specific_humidity_g_per_kg_7    0.51
station_min_temp_c_8                       0.51
station_avg_temp_c_7                       0.51
reanalysis_specific_humidity_g_per_kg_11   0.51
station_max_temp_c_11                      0.51
reanalysis_dew_point_temp_k_7              0.50
reanalysis_dew_point_temp_k_12             0.50
reanalysis_specific_humidity_g_per_kg_12   0.49
station_max_temp_c_10                      0.49
reanalysis_min_air_temp_k_9                0.49
station_min_temp_c_7                       0.49


sqrt y
station_avg_temp_c_10                      0.50
station_avg_temp_c_11                      0.50
station_avg_temp_c_12                      0.50
station_avg_temp_c_9                       0.49
station_avg_temp_c_8                       0.48
station_min_temp_c_10                      0.48
station_min_temp_c_11                      0.47
station_min_temp_c_9                       0.47
station_min_temp_c_12                      0.47
station_avg_temp_c_7                       0.46
station_min_temp_c_8                       0.46
station_min_temp_c_7                       0.45
reanalysis_specific_humidity_g_per_kg_8    0.45
station_max_temp_c_12                      0.45
reanalysis_dew_point_temp_k_9              0.45
reanalysis_specific_humidity_g_per_kg_9    0.45
reanalysis_dew_point_temp_k_8              0.45
station_max_temp_c_11                      0.44
reanalysis_dew_point_temp_k_10             0.44
reanalysis_specific_humidity_g_per_kg_10   0.44
station_avg_temp_c_6                       0.44
reanalysis_specific_humidity_g_per_kg_7    0.43
reanalysis_dew_point_temp_k_7              0.43
station_max_temp_c_10                      0.43
station_min_temp_c_6                       0.43
reanalysis_min_air_temp_k_9                0.42
reanalysis_specific_humidity_g_per_kg_6    0.42
reanalysis_dew_point_temp_k_11             0.42
'''

print('\nSAN JUAN MODEL TRAINING\n')
df_train = dfs_train[1]
def back_transform_sj_y(values):
    return np.power(values,2).round(0).astype(int)
    #return np.exp(values).round(0).astype(int)

#return x
def pre_process_sj(df,get_response=True,scaler = None):
   
    explanatory = [
'station_avg_temp_c_12',                  
'reanalysis_specific_humidity_g_per_kg_9',    
'reanalysis_dew_point_temp_k_9',          
'station_min_temp_c_11',                      
'station_max_temp_c_12',                     
'reanalysis_min_air_temp_k_9',                 
'reanalysis_air_temp_k_9',                    
'reanalysis_max_air_temp_k_10',                
'reanalysis_avg_temp_k_9', 
    ]
    #explanatory = df.columns
    
    
    x = df[explanatory].copy().values#.apply(lambda x: np.power(x))

    if scaler == None:
        scaler = preprocessing.StandardScaler().fit(x)
    x = scaler.transform(x) 
    
    x = pd.DataFrame(x,columns = [
'(station) Average temperature',                  
'(reanalysis) Specific humidity',    
'(reanalysis) Dew point temperature',          
'(station) Minimum temperature',                      
'(station) Maximum temperature',                     
'(reanalysis) Minimum air temperature',                 
'(reanalysis) Air temperature',                    
'(reanalysis) Maximum air temperature',                
'(reanalysis) Average temperature',   
     ])
    #poly = PolynomialFeatures(degree=2)
    #x = poly.fit_transform(x)

    if get_response:
        y = df[response].copy().apply(lambda x: np.sqrt(x))
        y = y.replace({-np.Inf: np.NaN})
        y = y.interpolate()
        return x,y,scaler
    return x,scaler

x_sj,y_sj,scaler_sj = pre_process_sj(df_train)
x = x_sj
y = y_sj



#usar para hexbin tbm
#scatter plot predictor x reponse 
len_side = 3
f, axarr = plt.subplots(int(len(x.columns)/len_side) + 1,len_side,
                        figsize=(13,8),sharey=True)

f.subplots_adjust(hspace=.4)
f.subplots_adjust(wspace=.1)
side = 0
index = 0
for predictor in x.columns:
    x_ = x[predictor]
    y_ = y
    #print(predictor,index,side)
    axarr[index,side].scatter(x_,y_)
    #hb = axarr[index,side].hexbin(x_,y_,bins=50,cmap="OrRd")#hexbin cmap='inferno')
    #cb = f.colorbar(hb, ax=axarr[index,side])
    axarr[index,side].set_xlabel(predictor)
    side += 1
    if side > len_side - 1:
        side = 0
        index+=1   
plt.suptitle("Total dengue fever cases by predictor")
plt.show()

'''
#correlations
x[response] = y
#df_train = df_train.apply(lambda x: np.sqrt(x))
#df_train[response] =  df_train[response].apply(lambda x: np.log(x))
corrmat = x.corr()[response]
print("Pearson correlation coefficient")
print(corrmat.sort_values(ascending = False)[:30])
'''

estimator = linear_model.Lasso(alpha=.1)
#estimator = linear_model.LinearRegression()


best_sj_model = train(x,y,estimator,back_transform_sj_y)


for name,coeff in zip(x.columns,best_sj_model.coef_):
    print(name,coeff)



f, axarr = plt.subplots(1,2,figsize=(10,5),sharey=True)
#plt.xticks(rotation=90)
i = 0
for range_x,city,x,y_true,model,back_transform in zip(
        [dfs_train[0].index,dfs_train[1].index],
        ["Iquitos","San Juan"],
        [x_iq,x_sj],
        [dfs_train[0][response],dfs_train[1][response]],
        [best_iq_model,best_sj_model],
        [back_transform_iq_y,back_transform_sj_y]):
    y_pred = back_transform(model.predict(x))
    axarr[i].plot(range_x,y_true,c="b", label = "Real Values")
    axarr[i].plot(range_x,y_pred,c="r", label = "Predicts")
    axarr[i].set_title(city)
    for tick in axarr[i].xaxis.get_major_ticks():
        tick.label.set_fontsize(10) 
        tick.label.set_rotation(30)

    #axarr[i].xaxis.set_rotation(90)
    i += 1
#f.autofmt_xdate()
plt.legend()
plt.show()



'''
#reading dfs
features_path2 = path + "dengue_features_test.csv"
df_test = pd.read_csv(features_path2)
dfs_test,old_len_predictors = pre_process_data(df = df_test)

#predictions
#para cada modelo

predictions = []
for df_test,best_model,process,back_transform in zip(dfs_test,[best_iq_model,best_sj_model],[pre_process_iq,pre_process_sj],[back_transform_iq_y,back_transform_sj_y]):
    df_test = df_test.copy()
    if (back_transform == back_transform_sj_y):
        to_predict,scaler = process(df_test,get_response=False,scaler=scaler_sj)    
    else:
        to_predict,scaler = process(df_test,get_response=False)
    df_test['total_cases'] = back_transform(best_model.predict(to_predict))
    predictions.append(df_test['total_cases'])
    
    fig = plt.figure(figsize=(26, 10), dpi=100)
    ticks = np.arange(len(df_test))
    plt.plot(ticks,df_test['total_cases'],c='r',label = "Predicts")
    plt.legend()
    plt.show()

#reverse pred -> sj iq   
predictions = predictions[::-1]


#SAVING SUMISSION FILE
submission = pd.read_csv(path + "submission_format.csv",
                         index_col=[0, 1, 2])
result_path = "result/"
submission.total_cases = np.concatenate(predictions)
submission.to_csv(result_path+"result.csv")
'''