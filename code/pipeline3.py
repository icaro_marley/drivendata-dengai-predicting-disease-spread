# -*- coding: utf-8 -*-
"""
Created on Wed May  3 02:19:40 2017


@author: icaromarley5
"""

'''
tentar melhorar o modelo de sj


Bell curves
 entender pq o outro funciona, melhorar.
 usar a divisão dele de dfs e cálculo de pontuação 
usar rfr ou lr

--usar bagging/boosting
--gridsearchcv
--improve randomstatemodel com fit e refit

pipeline sklearn
time series, 
pgm
fill na com valores próxima
escolher um melhor normalizador
''' 

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats
import seaborn as sns
import winsound
from sklearn.externals import joblib
from sklearn.ensemble import RandomForestRegressor
from sklearn import preprocessing

#preprocess data and returns 2 dataframes with Iquitos and San Juan Data
def pre_process_data(df,response=None,weeks_before=5): 
    #splitting to model the 2 cities
    df = df.copy()
    columns = ['week_start_date']
    df.drop(columns,axis=1,inplace=True)
    
    df_train_iq = df[df['city'] == 'iq'].copy()
    df_train_sj = df[df['city'] == 'sj'].copy()
    
    
  
    #getting values from past weeks
    dfs_train = [df_train_iq,df_train_sj]
    
    for df_train in dfs_train:
        df_train.drop(['city'],inplace=True,axis=1)
    
    
    columns = df_train_sj.columns.values
    if response != None:
        columns = np.delete(columns,df_train.columns.get_loc(response))

    old_len_columns = len(columns) 
    
    
    for n in range(1,weeks_before+1):
         for df_train in dfs_train:  
             for column in columns:
                 df_train[column+"_"+ str(n)] = df_train[column].shift(n)

    #dealing with na values -> fill with mean
    for df_train in dfs_train:
        df_train.fillna(df_train.mean(),inplace=True)

    if response != None:
        predictors = np.delete(df_train.columns,df_train.columns.get_loc(response))
    else:
        predictors = df_train.columns
    return dfs_train,predictors,old_len_columns


#reading training files
path = "../modified data/"
features_path = path + "dengue_features_train.csv"
labels_path = path + "dengue_labels_train.csv"

df_feat = pd.read_csv(features_path)
df_lab = pd.read_csv(labels_path)

#merge
df_merge = pd.merge(df_feat,df_lab,on=["city","year","weekofyear"])


#selecting response
response = "total_cases"

dfs_train,predictors,old_len_columns = pre_process_data(df_merge,response)


'''
#hist plot
f, axarr = plt.subplots(1,2,sharex=True,sharey=True,figsize=(7,4))
index = 0 
for city,df_train in zip(["Iquitos","San Juan"],dfs_train):
    f.subplots_adjust(hspace=.3)
    axarr[index].hist(df_train[response], 50)
    axarr[index].set_title(city)
    index+=1
plt.suptitle("Histogram of total dengue fever cases")
plt.show()

#line plot
f, axarr = plt.subplots(1,2,sharey=True,figsize=(7,4))
index = 0 
for city,df_train in zip(["Iquitos","San Juan"],dfs_train):
    f.subplots_adjust(hspace=.3)
    axarr[index].plot(df_train[response])
    axarr[index].set_title(city)
    index+=1
plt.suptitle("Line Plot of total dengue fever cases")
plt.show()

#correlations

for city,df_train in zip(["Iquitos","San Juan"],dfs_train):
    corrmat = df_train.corr()
    
    fig = plt.figure(figsize=(26, 10), dpi=100)
    ticks = np.arange(len(corrmat['total_cases']))
    plt.plot(ticks,corrmat['total_cases'])
    labels = corrmat['total_cases'].index.values
    plt.xticks(ticks, labels, rotation='vertical')
    plt.title(city)
    plt.show()


#usar para hexbin tbm
#scatter plot predictor x reponse by medition week 
len_side = 6
predictors_len = len(predictors)
for city,df_train in zip(["Iquitos","San Juan"],dfs_train):
    df_train.dropna(inplace=True)
    step = old_len_columns
    limit_inf = 0
    limit_max = step
    week = 0
    while (limit_max<=predictors_len):
        index = 0
        side = 0
        selected = predictors[limit_inf:limit_max]
        
        f, axarr = plt.subplots(int(len(selected)/len_side) + 1,len_side,
                                figsize=(19,14),sharey=True)
        f.subplots_adjust(hspace=.5)
        for predictor in selected:
            x = df_train[predictor].apply(lambda x:x)
            y = df_train[response].apply(lambda x: x)
            #print(predictor,index,side)
            axarr[index,side].scatter(x,y)#hexbin cmap='inferno')
            axarr[index,side].set_xlabel(predictor)
            corr = scipy.stats.pearsonr(x,y)
            axarr[index,side].set_title("Pearson R {:.2f} P {:.2f}".format(corr[0],corr[1]))
            side+=1
            if side > len_side - 1:
                side = 0
                index+=1   
        plt.suptitle(city + ": total dengue fever cases by predictors week " + str(week))
        plt.show()  
        
        #print(limit_inf,limit_max,selected,step)
        limit_inf = limit_max
        limit_max += step
        week+= -1



'''
'''
ANALYSIS

iquitos
few outliers, mais bem comportada
menos dados

as semanas anteriores tem mais relação linear com a variavel!!!
linear
pioram nas semanas anreriores principalmente 3 4 5

poly 2
week of year   ok
log > reanalysis dew point k ok 

station avg temp c ok
station min temp c ok


san juan, varia muito muitos picos
many outliers 
mais dados

poly 2
weekofyear 0 1 2 3 4  ok

variaveis da semana 3 4 5 -> 4 5
dew point temp
humidity g per kg
station avg temp c
station min temp c



log10 ou log x -> reanalysis min air temp k boa
log > reanalysis dew point k  e station_avg_temp_c avg temp k  air temp k


week of year
precipitation amt mm
reanalysis dew point temp k 2 3 4 5 -> log10 -> log1p
reanaçysis precip amt kg per m2  ok parece boa checar melhor
reanaçysis sat precip amt mm ok

other regression   ->>>>
 power 2 em x   vira poly 2
 reanalysis sat precip amt mm ok 
  station precip mm  ok
ndvi_ne ok
ndvi_nw pl
'''

#TRAINING

from sklearn.preprocessing import PolynomialFeatures,StandardScaler
from sklearn.model_selection import GridSearchCV
#train models with gridsearchcv
def train(x,y,estimator,params):
    clf = GridSearchCV(estimator, params,scoring="neg_mean_absolute_error",cv=10)#,verbose=10000000000)
    clf.fit(x,y)
    #print("best score",clf.best_score_)
    #print("best params",clf.best_params_)
    #winsound.Beep(300,300)
    return clf.best_estimator_,clf.best_score_  

def train_final(x,y,best_model):
    best_model = RandomForestRegressor(**best_model.get_params())
    best_model.fit(x,y)
    return best_model
    



'''
#IQUITOS MODEL  RFR  -7.32855769231
df_train = dfs_train[0].copy()


explanatory = [
 'station_avg_temp_c',
 'station_min_temp_c',
  'weekofyear']

x = df_train[explanatory].copy()
y = df_train[response].copy()
scaler = preprocessing.StandardScaler().fit(x)
x = scaler.transform(x) 


estimator = RandomForestRegressor(random_state=100,criterion='mae')
params = {}
best_iq_model,score = train(x,y,estimator,params)
print(score)


best_iq_model = train_final(x,y,best_iq_model)

explanatory_iq = explanatory

estimator = best_iq_model

model_path = "models/"
joblib.dump(estimator, model_path+ 'THIRDSUB_RFR_IQ.pkl') 

'''

df_train = dfs_train[0].copy()
explanatory = [
 'station_avg_temp_c',
 'station_min_temp_c',
  'weekofyear']
x_iq = df_train[explanatory]
scaler = StandardScaler().fit(x_iq)
x_iq = scaler.transform(x_iq)
explanatory_iq = explanatory
model_path = "models/"
best_iq_model = joblib.load(model_path + 'THIRDSUB_RFR_IQ.pkl')



#SAN JUAN  MODEL  SVR LINEAR  -21.317193218701124 
#list(df_train.columns)
'''
'weekofyear_1',#22

'station_min_temp_c_5',#24

'reanalysis_min_air_temp_k_5'#24 log1p
'''


'''

df_train = dfs_train[1].copy()

explanatory = [
'weekofyear_1',]


x = df_train[explanatory]
y = df_train[response]
scaler = StandardScaler().fit(x)
x = scaler.transform(x)  

estimator = RandomForestRegressor(random_state=100,criterion='mae')
params = {}
best_sj_model,score = train(x,y,estimator,params)

print(score)

best_sj_model = train_final(x,y,best_sj_model)

explanatory_sj = explanatory
estimator = best_sj_model

model_path = "models/"
joblib.dump(estimator, model_path+ 'THIRDSUB_RFR_SJ.pkl')

'''

df_train = dfs_train[1].copy()
explanatory = ['weekofyear']
x_sj = df_train[explanatory]
scaler = StandardScaler().fit(x_sj)
x_sj = scaler.transform(x_sj)  
explanatory_sj = explanatory
model_path = "models/"
best_sj_model = joblib.load(model_path + 'THIRDSUB_RFR_SJ.pkl')



"""
#feature selection (backwards elimination)  svr linear
best_score = -5.6472386594704211 
explanatory = list(predictors)
old_len = 0
new_len = len(explanatory)
while (old_len != new_len):
    print("TENTANDO NOVAMENTE")
    i = 0
    while (i<len(explanatory)):
        old_len = new_len
        print(i)
        selected = explanatory.pop(i)
     
        x = df_train[explanatory].copy()
        y = df_train[response].copy()
        scaler = preprocessing.StandardScaler().fit(x)
        x = scaler.transform(x) 
        estimator = SVR(kernel='linear',C=4, epsilon = 0.089999999999999997,tol=0.00092000000000000014)
        params = {}
        best_iq_model,score = train(x,y,estimator,params)
        if score > best_score:
            best_score = score
            print("best score so far",best_score,"len",len(explanatory))
        else:
            explanatory.insert(i,selected)
            i+=1
    
    new_len = len(explanatory)
  
""" 

'''
#reading dfs
features_path2 = path + "dengue_features_test.csv"
df_test = pd.read_csv(features_path2)
dfs_test,predictors,old_len_predictors = pre_process_data(df = df_test)

#predictions
#para cada modelo

predictions = []
for df_test,best_model,x in zip(dfs_test,[best_iq_model,best_sj_model],[explanatory_iq,explanatory_sj]):
    to_predict = df_test[x]
    scaler = preprocessing.StandardScaler().fit(to_predict)
    to_predict = scaler.transform(to_predict) 
    df_test['total_cases'] = best_model.predict(to_predict)
    predictions.append(df_test['total_cases'].astype(int))
    
    fig = plt.figure(figsize=(26, 10), dpi=100)
    ticks = np.arange(len(df_test))
    plt.plot(ticks,df_test['total_cases'],c='r',label = "Predicts")
    plt.legend()
    plt.show()
    
    

#SAVING SUMISSION FILE
submission = pd.read_csv(path + "submission_format.csv",
                         index_col=[0, 1, 2])
result_path = "result/"
submission.total_cases = np.concatenate(predictions)
submission.to_csv(result_path+"result.csv")
'''

'''
#plotando valores reais contra previsão

for df_train,best_model,x in zip(dfs_train,[best_iq_model,best_sj_model],[x_iq,x_sj]):
    fig = plt.figure(figsize=(26, 10), dpi=100)
    ticks = np.arange(len(df_train))
    plt.plot(ticks,df_train['total_cases'],c='b',label = "Real Values")
    plt.plot(ticks,best_model.predict(x),c='r',label = "Predicts")
    plt.legend()
    plt.show()
'''