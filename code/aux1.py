'''
linear regression
log on x
sqrt on y
scale -> poly degree 2
variables
    explanatory = [
    'reanalysis_min_air_temp_k_5',
    'station_min_temp_c_9',
    'reanalysis_air_temp_k_9',
    'reanalysis_tdtr_k_4',
    'station_diur_temp_rng_c_2'        
    ]
lreg poly 2 -1.71796358226 -> 15.7260157273

 'reanalysis_tdtr_k_4',-> 13.9357841833
 
 
 
 
root 4th 10.0772355503
    'reanalysis_tdtr_k_4',
    'station_diur_temp_rng_c_2'    10.0165757217    

'''

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats
import seaborn as sns
import winsound
from sklearn.externals import joblib
from sklearn.ensemble import RandomForestRegressor
from sklearn import preprocessing
from sklearn.preprocessing import PolynomialFeatures
from sklearn import linear_model
from sklearn.preprocessing import PolynomialFeatures,StandardScaler
from sklearn.model_selection import GridSearchCV

#preprocess data and returns 1 df
def pre_process_data(df,response=None,weeks_before=15): 
    #splitting to model the 2 cities
    df = df.copy()

    columns = ['week_start_date']
    df.drop(columns,axis=1,inplace=True)
    

    '''
    if response != None:
        #replace this values with mean
        #df_train_sj = df_train_sj[df_train_sj[response]<90]###linha adicionada
        mean_outlier =  df_train_sj[response][(df_train_sj[response]>50) & (df_train_sj[response]<90)].mean()
        df_train_sj.loc[df_train_sj[response]>90,response] = mean_outlier + df_train_sj[response][df_train_sj[response]>90].mod(30)
        #print(df_train_sj[response][df_train_sj[response]>60].mean())#71
    '''  
    
    columns = df.columns.values
    if response != None:
        columns = np.delete(columns,df.columns.get_loc(response))
        columns = np.delete(columns,df.columns.get_loc('city'))
  
    old_len_columns = len(columns) 

    #dealing with na values 
    df.fillna(method='ffill',inplace=True) 
    
    for n in range(1,weeks_before+1):
        for column in columns:
            df[column+"_"+ str(n)] = df[column].shift(n)

    #dealing with na values for the shift operation
    df.fillna(method='bfill',inplace=True)#columns shifted 
    
    #one hot encoding for city
    df_city = pd.get_dummies(df['city']).astype(float)
    df.drop(['city'], axis = 1, inplace = True)
    df = df_city.join(df)
    
    if response != None:
        predictors = np.delete(df.columns,df.columns.get_loc(response))
    else:
        predictors = df.columns
    return df,predictors,old_len_columns



#reading training files
path = "../modified data/"
features_path = path + "dengue_features_train.csv"
labels_path = path + "dengue_labels_train.csv"

df_feat = pd.read_csv(features_path)
df_lab = pd.read_csv(labels_path)

#merge
df_merge = pd.merge(df_feat,df_lab,on=["city","year","weekofyear"])


#selecting response
response = "total_cases"

df_train,predictors,old_len_columns = pre_process_data(df_merge,response)


'''
#hist
f, axarr = plt.subplots(figsize=(7,4))
index = 0 
f.subplots_adjust(hspace=.3)
axarr.hist(df_train[response],50)
    
plt.title("Histogram of total dengue fever cases")
plt.show()


#correlations
corrmat = df_train.corr()
fig = plt.figure(figsize=(46, 10), dpi=100)
ticks = np.arange(len(corrmat['total_cases']))
plt.plot(ticks,corrmat['total_cases'])
labels = corrmat['total_cases'].index.values
plt.xticks(ticks, labels, rotation='vertical')
plt.show()





#usar para hexbin tbm
#scatter plot predictor x reponse by medition week 
len_side = 6
predictors_len = len(predictors)

step = old_len_columns 
limit_inf = 0
limit_max = step
week = 0
numeric_pred = predictors[2:]
while (limit_max<=predictors_len):
    index = 0
    side = 0
    selected = numeric_pred[limit_inf:limit_max]
    
    f, axarr = plt.subplots(int(len(selected)/len_side) + 1,len_side,
                            figsize=(20,8),sharey=True)
    f.subplots_adjust(hspace=.8)
    f.subplots_adjust(wspace=.5)
    for predictor in selected:
        x = df_train[predictor]#.apply(lambda x:np.power(x,1/3))#.apply(lambda x: np.power(x,1/3))
        y = df_train[response]#.apply(lambda x: np.log(x))
        #print(predictor,index,side)
        axarr[index,side].scatter(x,y)#hexbin cmap='inferno')
        axarr[index,side].set_xlabel(predictor)
        corr = scipy.stats.pearsonr(x,y)
        axarr[index,side].set_title("Pearson R {:.2f} P {:.2f}".format(corr[0],corr[1]))
        side+=1
        if side > len_side - 1:
            side = 0
            index+=1   
    plt.suptitle(": total dengue fever cases by predictors week " + str(week))
    plt.show()  
    
    #print(limit_inf,limit_max,selected,step)
    limit_inf += step
    limit_max += step
    week+= -1


-1.73028229922 rfr
-1.73844767969 lreg
-1.71796358226 lreg poly 2


'''

n_root = 2
#scoring function]
from sklearn.metrics import mean_absolute_error
scoring = lambda estimator,x,y_pred: mean_absolute_error(back_transform_y(estimator.predict(x)),y_pred)

#training
def train(x,y,estimator,params):
    clf = GridSearchCV(estimator, params,scoring=scoring,cv=10)#,verbose=10000000000)
    clf.fit(x,y)
    return clf.best_estimator_,clf.best_score_  


def back_transform_y(values):
    return np.power(values.round(0).astype(int),n_root)

#return x,y
def pre_process(df,get_response=True):   
    explanatory = [
    #'reanalysis_min_air_temp_k_5',
    #'station_min_temp_c_9',
    #'reanalysis_air_temp_k_9',
    'reanalysis_tdtr_k_4',
    #'station_diur_temp_rng_c_2'        
    ]

    
    x = df[explanatory].copy()
    
    x = x.apply(lambda x: np.log(x))
    scaler = preprocessing.StandardScaler().fit(x)
    x = scaler.transform(x) 
    poly = PolynomialFeatures(degree=2)#4
    x = poly.fit_transform(x)


    if get_response:
        y = df[response].copy()
        y = y.apply(lambda x: np.power(x,1/n_root))
        return x,y
    return x

x,y = pre_process(df_train)

estimator = linear_model.LinearRegression()
params = {
}

best_model,score = train(x,y,estimator,params)
print(score)




'''
#feature selection (backwards elimination) rfr
best_score = -17.6616619913
x,y = pre_process(df_train)
explanatory = list(x.columns)
old_len = 0
new_len = len(explanatory)
while (old_len != new_len):
    print("TENTANDO NOVAMENTE")
    i = 0
    while (i<len(explanatory)):
        old_len = new_len
        print(i)
        selected = explanatory.pop(i)
        x_local = x[explanatory]

        estimator = estimator = RandomForestRegressor(
        oob_score=False,
        min_weight_fraction_leaf = 0.12700000000000003,
        min_impurity_split = 1.0001e-06,
        n_estimators=535,
        random_state= 100,
        #criterion='mae'
        )
        params = {}
        best__model,score = train(x,y,estimator,params)
        if score >= best_score:
            best_score = score
            print("best score so far",best_score,"len",len(explanatory))
        else:
            explanatory.insert(i,selected)
            i+=1
    
    new_len = len(explanatory)
  

print(best_score)
print(explanatory)
winsound.Beep(300,300)

'''






















'''
#PCA
from sklearn.preprocessing import scale
from sklearn.decomposition import PCA

pca = PCA()
x =  np.delete(df_train.columns.copy(),df_train.columns.get_loc(response))
x = df_train[x]


scaler = preprocessing.StandardScaler().fit(x)
x = scaler.transform(x) 

x_reduced = pca.fit_transform(scale(x))

list(np.cumsum(np.round(pca.explained_variance_ratio_, decimals=100)*100))

i = 1
x_reduced
scores = []
n = 3
while i < len(x_reduced) and i <= n:
    print(i)
    x = x_reduced[:,:i]
    #random forest
    estimator = RandomForestRegressor(
    oob_score=False,
    min_weight_fraction_leaf = 0.12700000000000003,
    min_impurity_split = 1.0001e-06,
    n_estimators=535,
    random_state= 100,
    )
    
    params = {
    }
    
    best_model,score = train(x,y,estimator,params)

    scores.append(score)
    i+=1

#line plot
plt.plot(list(range(1,n+1)),scores)
plt.show()


x_reduced = pd.DataFrame(x_reduced)


'''


#plot line plot from predictions 
#and real values
'''
#plotando valores reais contra previsão
fig = plt.figure(figsize=(26, 10), dpi=100)
ticks = np.arange(len(df_train))
y_true = df_train['total_cases']
y_pred = back_transform_y(best_model.predict(x))
plt.plot(ticks,y_true,c='b',label = "Real Values")
plt.plot(ticks,y_pred,c='r',label = "Predicts")
plt.legend()
plt.show()

from sklearn.metrics import mean_absolute_error
print(mean_absolute_error(y_true,y_pred))




'''
'''
#reading dfs
features_path2 = path + "dengue_features_test.csv"
df_test = pd.read_csv(features_path2)
df_test,predictors,old_len_predictors = pre_process_data(df = df_test)


#predictions
#para cada modelo

predictions = []
for best_model,process,back_transform in zip([best_model],[pre_process],[back_transform_y]):
    df_test = df_test.copy()
    to_predict = process(df_test,get_response=False)
    df_test['total_cases'] = back_transform(best_model.predict(to_predict))
    predictions.append(df_test['total_cases'])
    
    fig = plt.figure(figsize=(20, 10), dpi=100)
    ticks = np.arange(len(df_test))
    plt.plot(ticks,df_test['total_cases'],c='r',label = "Predicts")
    plt.legend()
    plt.show()
    

#SAVING SUMISSION FILE
submission = pd.read_csv(path + "submission_format.csv",
                         index_col=[0, 1, 2])
result_path = "result/"
submission.total_cases = np.concatenate(predictions)
submission.to_csv(result_path+"result.csv")
'''

